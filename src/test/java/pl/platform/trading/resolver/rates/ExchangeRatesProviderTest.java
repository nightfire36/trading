package pl.platform.trading.resolver.rates;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import pl.platform.trading.config.BasicConfig;
import pl.platform.trading.entity.ExchangeRate;
import testutils.PrivateFieldSetter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

class ExchangeRatesProviderTest {

    @Mock
    private BasicConfig basicConfig;

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private ExchangeRatesProvider exchangeRatesProvider;


    private final String testRatesProviderUrl = "exampleprovider.com";


    @BeforeEach
    void testSetUp() throws NoSuchFieldException, IllegalAccessException {
        MockitoAnnotations.openMocks(this);

        PrivateFieldSetter.setField(
                ExchangeRatesProvider.class, "restTemplate", exchangeRatesProvider, restTemplate
        );

        List<ExchangeRateDto> ratesResponse = new ArrayList<>();
        ratesResponse.add(createExchangeRateDto("GBPUSD", "1.3689", "1.3735"));
        ratesResponse.add(createExchangeRateDto("EURPLN", "4.4498", "1.4528"));
        ratesResponse.add(createExchangeRateDto("USDCAD", "1.2476", "1.2602"));
        ratesResponse.add(createExchangeRateDto("USDJPY", "110.7465", "112.8463"));
        ratesResponse.add(createExchangeRateDto("UNKNRT", "1.9536", "1.9747"));

        Mockito.when(basicConfig.getRatesProviderUrl())
                .thenReturn(testRatesProviderUrl);
        Mockito.when(
                restTemplate.exchange(
                        testRatesProviderUrl,
                        HttpMethod.GET,
                        HttpEntity.EMPTY,
                        new ParameterizedTypeReference<List<ExchangeRateDto>>() {}
                )
        ).thenReturn(ResponseEntity.ok(ratesResponse));
    }

    private ExchangeRateDto createExchangeRateDto(
            String pair, String bid, String ask
    ) throws NoSuchFieldException, IllegalAccessException {
        ExchangeRateDto rateDto = new ExchangeRateDto();
        PrivateFieldSetter.setField(ExchangeRateDto.class, "currencyPair", rateDto, pair);
        PrivateFieldSetter.setField(ExchangeRateDto.class, "bid", rateDto, new BigDecimal(bid));
        PrivateFieldSetter.setField(ExchangeRateDto.class, "ask", rateDto, new BigDecimal(ask));
        PrivateFieldSetter.setField(ExchangeRateDto.class, "min", rateDto, new BigDecimal("1.8739"));
        PrivateFieldSetter.setField(ExchangeRateDto.class, "max", rateDto, new BigDecimal("1.9241"));
        PrivateFieldSetter.setField(ExchangeRateDto.class, "timestamp", rateDto, new Timestamp(222222222L));
        PrivateFieldSetter.setField(ExchangeRateDto.class, "chartDirection", rateDto, -3);
        return rateDto;
    }

    @Test
    void updateRates() {
        // when
        exchangeRatesProvider.updateRates();
        List<ExchangeRate> rates = exchangeRatesProvider.getRates();

        // then
        Mockito.verify(basicConfig).getRatesProviderUrl();
        Mockito.verify(restTemplate).exchange(
                testRatesProviderUrl,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<ExchangeRateDto>>() {}
        );

        Assertions.assertEquals(4, rates.size());
        Assertions.assertEquals("GBPUSD", rates.get(0).getCurrencyPair());
        Assertions.assertEquals("EURPLN", rates.get(1).getCurrencyPair());
        Assertions.assertEquals("USDCAD", rates.get(2).getCurrencyPair());
        Assertions.assertEquals("USDJPY", rates.get(3).getCurrencyPair());
        Assertions.assertEquals(new BigDecimal("110.7465"), rates.get(3).getBid());
        Assertions.assertEquals(new BigDecimal("112.8463"), rates.get(3).getAsk());
        Assertions.assertEquals(new Timestamp(222222222L), rates.get(3).getTimestamp());
    }

    @Test
    void getPairRateKnown() {
        // when
        exchangeRatesProvider.updateRates();
        ExchangeRate rate = exchangeRatesProvider.getPairRate("USDCAD");

        // then
        Assertions.assertEquals("USDCAD", rate.getCurrencyPair());
        Assertions.assertEquals(new BigDecimal("1.2476"), rate.getBid());
        Assertions.assertEquals(new BigDecimal("1.2602"), rate.getAsk());
        Assertions.assertEquals(new Timestamp(222222222L), rate.getTimestamp());
    }

    @Test
    void getPairRateUnknown() {
        // when
        exchangeRatesProvider.updateRates();
        ExchangeRate rate = exchangeRatesProvider.getPairRate("UNKNRT");

        // then
        Assertions.assertNull(rate);
    }
}
