package pl.platform.trading.resolver;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.platform.trading.entity.ExchangeRate;
import pl.platform.trading.entity.position.OpenedPosition;

import java.math.BigDecimal;

class ProfitCalculatorTest {

    @Test
    void calculateProfit() {
        OpenedPosition opened = OpenedPosition.builder()
                .amount(new BigDecimal("150"))
                .openingPrice(new BigDecimal("1.125"))
                .longPosition(true)
                .build();

        ExchangeRate rate = ExchangeRate.builder()
                .currencyPair("EURCHF")
                .ask(new BigDecimal("1.245"))
                .bid(new BigDecimal("1.241"))
                .build();

        BigDecimal profit = ProfitCalculator.calculateProfit(opened, rate);

        Assertions.assertEquals(new BigDecimal("15.46667"), profit);
    }
}