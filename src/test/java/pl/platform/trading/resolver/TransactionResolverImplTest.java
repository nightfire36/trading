package pl.platform.trading.resolver;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import pl.platform.trading.entity.ExchangeRate;
import pl.platform.trading.entity.User;
import pl.platform.trading.entity.order.ClosingOrder;
import pl.platform.trading.entity.order.OrderStatus;
import pl.platform.trading.entity.position.ForexPosition;
import pl.platform.trading.entity.position.OpenedPosition;
import pl.platform.trading.repository.UserRepository;
import pl.platform.trading.repository.order.ClosingOrderRepository;
import pl.platform.trading.repository.order.OpeningOrderRepository;
import pl.platform.trading.repository.position.OpenedPositionRepository;
import pl.platform.trading.resolver.rates.ExchangeRatesProvider;
import testutils.PrivateFieldSetter;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;

class TransactionResolverImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private OpeningOrderRepository openingOrderRepository;

    @Mock
    private ClosingOrderRepository closingOrderRepository;

    @Mock
    private OpenedPositionRepository openedPositionRepository;

    @Mock
    private ExchangeRatesProvider rates;

    @InjectMocks
    private TransactionResolverImpl resolver;


    private final Long testUserId = 3L;
    private final Long testOpenedPositionId = 15L;

    private final User testUser = User.builder()
            .firstName("ExampleFirstName")
            .lastName("ExampleLastName")
            .email("example_user@mail.com")
            .accountBalance(new BigDecimal("20500"))
            .build();

    private final OpenedPosition testOpenedPosition = OpenedPosition.builder()
            .currencyPair("GBPCAD")
            .longPosition(false)
            .userId(testUserId)
            .amount(new BigDecimal("3500.85"))
            .openingPrice(new BigDecimal("1.4529"))
            .build();

    private final ClosingOrder testClosingOrder = ClosingOrder.builder()
            .userId(testUserId)
            .forexPositionId(testOpenedPositionId)
            .orderPrice(new BigDecimal("1.6000"))
            .triggeredAbove(true)
            .build();

    private final ExchangeRate testRate = ExchangeRate.builder()
            .currencyPair("GBPCAD")
            .ask(new BigDecimal("1.5841"))
            .bid(new BigDecimal("1.5824"))
            .build();


    @BeforeEach
    void testSetUp() throws NoSuchFieldException, IllegalAccessException {
        MockitoAnnotations.openMocks(this);

        PrivateFieldSetter.setField(User.class, "id", testUser, testUserId);
        PrivateFieldSetter.setField(ForexPosition.class, "id", testOpenedPosition, testOpenedPositionId);

        Mockito.when(userRepository.findById(testUserId))
                .thenReturn(Optional.of(testUser));
        Mockito.when(openedPositionRepository.findById(testOpenedPositionId))
                .thenReturn(Optional.of(testOpenedPosition));
        Mockito.when(closingOrderRepository.findByForexPositionId(testOpenedPositionId))
                .thenReturn(Collections.singletonList(testClosingOrder));
        Mockito.when(rates.getPairRate("GBPCAD"))
                .thenReturn(testRate);
    }

    @Test
    void updateAccountBalance() {
        // given
        BigDecimal initialAmount = testUser.getAccountBalance();
        BigDecimal updateAmount = new BigDecimal("30.952");

        // when
        resolver.updateAccountBalance(testUserId, updateAmount);

        // then
        Mockito.verify(userRepository)
                .findById(testUserId);

        Assertions.assertEquals(initialAmount.add(updateAmount), testUser.getAccountBalance());
    }

    @Test
    void openPosition() {
        // given
        BigDecimal initialAmount = testUser.getAccountBalance();
        BigDecimal positionAmount = new BigDecimal("177.2395");
        String positionCurrencyPair = "GBPCAD";

        ArgumentCaptor<OpenedPosition> openedPositionCaptor = ArgumentCaptor.forClass(OpenedPosition.class);

        // when
        resolver.openPosition(testUserId, positionCurrencyPair, positionAmount, true);

        // then
        Mockito.verify(openedPositionRepository)
                .save(openedPositionCaptor.capture());

        OpenedPosition opened = openedPositionCaptor.getValue();
        Assertions.assertEquals(testUserId, opened.getUserId());
        Assertions.assertEquals(positionCurrencyPair, opened.getCurrencyPair());
        Assertions.assertEquals(positionAmount, opened.getAmount());
        Assertions.assertEquals(true, opened.getLongPosition());
        Assertions.assertEquals(testRate.getAsk(), opened.getOpeningPrice());

        Assertions.assertEquals(initialAmount.subtract(positionAmount), testUser.getAccountBalance());
    }

    @Test
    void closePosition() {
        // when
        resolver.closePosition(testOpenedPositionId);

        // then
        Mockito.verify(openedPositionRepository)
                .findById(testOpenedPositionId);
        Mockito.verify(rates)
                .getPairRate(testOpenedPosition.getCurrencyPair());
        Mockito.verify(closingOrderRepository)
                .findByForexPositionId(testOpenedPositionId);
        Mockito.verify(openedPositionRepository)
                .closePosition(
                        testRate.getAsk(),
                        new BigDecimal("-289.95109"),
                        testOpenedPositionId
                );

        Assertions.assertEquals(new BigDecimal("23710.89891"), testUser.getAccountBalance());
        Assertions.assertEquals(OrderStatus.SYSTEM_CANCELED, testClosingOrder.getOrderStatus());
    }

    @Test
    void resolve() {
        // given
        Mockito.when(rates.getRates())
                .thenReturn(Collections.singletonList(testRate));
        Mockito.when(openingOrderRepository.findOrdersToExecute(any(), any(), any(), any()))
                .thenReturn(Collections.emptyList());
        Mockito.when(closingOrderRepository.findOrdersToExecute(any(), any(), any(), any()))
                .thenReturn(Collections.emptyList());

        // when
        resolver.resolve();

        // then
        Mockito.verify(rates)
                .getRates();
        Mockito.verify(openingOrderRepository)
                .findOrdersToExecute(
                        testRate.getCurrencyPair(),
                        testRate.getBid(),
                        testRate.getAsk(),
                        OrderStatus.PENDING_ORDER
                );
        Mockito.verify(closingOrderRepository)
                .findOrdersToExecute(
                        testRate.getCurrencyPair(),
                        testRate.getBid(),
                        testRate.getAsk(),
                        OrderStatus.PENDING_ORDER
                );
    }
}