package pl.platform.trading.resolver;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.platform.trading.config.BasicConfig;
import pl.platform.trading.controller.message.request.UserDto;
import pl.platform.trading.entity.ExchangeRate;
import pl.platform.trading.entity.User;
import pl.platform.trading.entity.order.ClosingOrder;
import pl.platform.trading.entity.order.OpeningOrder;
import pl.platform.trading.entity.order.OrderStatus;
import pl.platform.trading.entity.position.OpenedPosition;
import pl.platform.trading.repository.UserRepository;
import pl.platform.trading.repository.order.ClosingOrderRepository;
import pl.platform.trading.repository.order.OpeningOrderRepository;
import pl.platform.trading.repository.position.OpenedPositionRepository;
import pl.platform.trading.resolver.rates.ExchangeRatesProvider;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

class PlatformModelImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private BasicConfig basicConfig;

    @Mock
    private OpenedPositionRepository openedPositionRepository;

    @Mock
    private ExchangeRatesProvider rates;

    @Mock
    private OpeningOrderRepository openingOrderRepository;

    @Mock
    private ClosingOrderRepository closingOrderRepository;

    @Mock
    private TransactionResolver resolver;

    @InjectMocks
    private PlatformModelImpl platformModel;


    private final Long currentUserId = 3L;

    private final OpenedPosition openedPosition1 = OpenedPosition.builder()
            .userId(currentUserId)
            .currencyPair("GBPUSD")
            .amount(new BigDecimal("1208.15"))
            .longPosition(true)
            .openingPrice(new BigDecimal("1.1853"))
            .build();

    private final OpenedPosition openedPosition2 = OpenedPosition.builder()
            .userId(currentUserId)
            .currencyPair("USDCAD")
            .amount(new BigDecimal("3580.90"))
            .longPosition(false)
            .openingPrice(new BigDecimal("1.2373"))
            .build();


    @BeforeEach
    void testSetUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void registerUser() {
        // given
        final String firstName = "ExampleFirstName";
        final String lastName = "ExampleLastName";
        final String email = "example_user@mail.com";
        final String password = "password12345";
        final String secretSalt = "secretSalt";

        UserDto userDto = new UserDto();
        userDto.setFirstName(firstName);
        userDto.setLastName(lastName);
        userDto.setEmail(email);
        userDto.setPassword(password);

        Mockito.when(basicConfig.getAdditionalSecretSalt())
                .thenReturn(secretSalt);

        ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);

        // when
        Integer registrationStatus = platformModel.registerUser(userDto);

        // then
        Mockito.verify(userRepository)
                .save(userCaptor.capture());

        User savedUser = userCaptor.getValue();
        Assertions.assertEquals(firstName, savedUser.getFirstName());
        Assertions.assertEquals(lastName, savedUser.getLastName());
        Assertions.assertEquals(email, savedUser.getEmail());
        Assertions.assertTrue(
                new BCryptPasswordEncoder()
                        .matches(password + "." + secretSalt, savedUser.getPassword())
        );
        Assertions.assertEquals(new BigDecimal("100000.0"), savedUser.getAccountBalance());

        Assertions.assertEquals(ModelStatusCode.STATUS_SUCCESS, registrationStatus);
    }

    @Test
    void getOpenedPositions() {
        // given
        final ExchangeRate rate1 = ExchangeRate.builder()
                .currencyPair("GBPUSD")
                .bid(new BigDecimal("1.3689"))
                .ask(new BigDecimal("1.3735"))
                .timestamp(new Timestamp(222222222L))
                .build();

        final ExchangeRate rate2 = ExchangeRate.builder()
                .currencyPair("USDCAD")
                .bid(new BigDecimal("1.2476"))
                .ask(new BigDecimal("1.2602"))
                .timestamp(new Timestamp(222222222L))
                .build();

        Mockito.when(openedPositionRepository.findByUserId(currentUserId))
                .thenReturn(Arrays.asList(openedPosition1, openedPosition2));
        Mockito.when(rates.getPairRate(rate1.getCurrencyPair()))
                .thenReturn(rate1);
        Mockito.when(rates.getPairRate(rate2.getCurrencyPair()))
                .thenReturn(rate2);

        // when
        List<OpenedPosition> openedPositions = platformModel.getOpenedPositions(currentUserId);

        // then
        Mockito.verify(openedPositionRepository)
                .findByUserId(currentUserId);
        Mockito.verify(rates)
                .getPairRate(rate1.getCurrencyPair());
        Mockito.verify(rates)
                .getPairRate(rate2.getCurrencyPair());

        Assertions.assertEquals(openedPosition1, openedPositions.get(0));
        Assertions.assertEquals(openedPosition2, openedPositions.get(1));
    }

    @Test
    void placeOpeningOrder() {
        // given
        final String currencyPair = "GBPUSD";
        final BigDecimal amount = new BigDecimal("1208.15");
        final BigDecimal orderPrice = new BigDecimal("1.1853");
        final boolean triggeredAbove = true;
        final boolean longPosition = true;

        ArgumentCaptor<OpeningOrder> openingOrderCaptor = ArgumentCaptor.forClass(OpeningOrder.class);

        // when
        Integer statusCode = platformModel.placeOpeningOrder(
                currentUserId, currencyPair, amount, triggeredAbove, orderPrice, longPosition
        );

        // then
        Mockito.verify(openingOrderRepository)
                .save(openingOrderCaptor.capture());
        Mockito.verify(resolver)
                .resolve();

        OpeningOrder savedOpeningOrder = openingOrderCaptor.getValue();
        Assertions.assertEquals(currentUserId, savedOpeningOrder.getUserId());
        Assertions.assertEquals(currencyPair, savedOpeningOrder.getCurrencyPair());
        Assertions.assertEquals(amount, savedOpeningOrder.getAmount());
        Assertions.assertEquals(orderPrice, savedOpeningOrder.getOrderPrice());
        Assertions.assertEquals(longPosition, savedOpeningOrder.getLongPosition());
        Assertions.assertEquals(triggeredAbove, savedOpeningOrder.getTriggeredAbove());

        Assertions.assertEquals(ModelStatusCode.STATUS_SUCCESS, statusCode);
    }

    @Test
    void placeClosingOrder() {
        // given
        final Long positionId = 3L;
        final BigDecimal price = new BigDecimal("1.1853");
        final boolean trigger = false;

        Mockito.when(openedPositionRepository.findById(positionId))
                .thenReturn(Optional.ofNullable(openedPosition1));

        ArgumentCaptor<ClosingOrder> closingOrderCaptor = ArgumentCaptor.forClass(ClosingOrder.class);

        // when
        Integer statusCode = platformModel.placeClosingOrder(
                currentUserId, positionId, trigger, price
        );

        // then
        Mockito.verify(openedPositionRepository)
                .findById(positionId);
        Mockito.verify(closingOrderRepository)
                .save(closingOrderCaptor.capture());
        Mockito.verify(resolver)
                .resolve();

        ClosingOrder savedClosingOrder = closingOrderCaptor.getValue();
        Assertions.assertEquals(currentUserId, savedClosingOrder.getUserId());
        Assertions.assertEquals(positionId, savedClosingOrder.getForexPositionId());
        Assertions.assertEquals(price, savedClosingOrder.getOrderPrice());
        Assertions.assertEquals(trigger, savedClosingOrder.getTriggeredAbove());

        Assertions.assertEquals(ModelStatusCode.STATUS_SUCCESS, statusCode);
    }

    @Test
    void cancelOrder() {
        // given
        final Long orderId = 17L;

        final ClosingOrder closingOrder = ClosingOrder.builder()
                .userId(currentUserId)
                .forexPositionId(3L)
                .orderPrice(new BigDecimal("1208.15"))
                .triggeredAbove(true)
                .build();

        Mockito.when(closingOrderRepository.findById(orderId))
                .thenReturn(Optional.ofNullable(closingOrder));

        // when
        Integer statusCode = platformModel.cancelOrder(currentUserId, orderId);

        // then
        Mockito.verify(openingOrderRepository)
                .findById(orderId);
        Mockito.verify(closingOrderRepository)
                .findById(orderId);

        Assertions.assertEquals(OrderStatus.CANCELED, closingOrder.getOrderStatus());
        Assertions.assertEquals(ModelStatusCode.STATUS_SUCCESS, statusCode);
    }

    @Test
    void cancelOrderWithInvalidId() {
        // given
        final Long orderId = 27L;

        // when
        Assertions.assertThrows(
                NoSuchElementException.class,
                () -> platformModel.cancelOrder(currentUserId, orderId)
        );
    }
}