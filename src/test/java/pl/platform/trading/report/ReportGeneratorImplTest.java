package pl.platform.trading.report;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import pl.platform.trading.controller.message.request.OrdersReportFilterDto;
import pl.platform.trading.controller.message.request.PositionsReportFilterDto;
import pl.platform.trading.entity.order.ClosingOrderForReportGeneration;
import pl.platform.trading.entity.order.OpeningOrder;
import pl.platform.trading.entity.order.OrderStatus;
import pl.platform.trading.entity.order.TransactionOrder;
import pl.platform.trading.entity.position.ForexPosition;
import pl.platform.trading.entity.position.OpenedPosition;
import pl.platform.trading.repository.order.ArchivedOrderRepository;
import pl.platform.trading.repository.position.ArchivedPositionRepository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import static testutils.PrivateFieldSetter.setField;

class ReportGeneratorImplTest {

    @Mock
    private ArchivedPositionRepository archivedPositionRepository;

    @Mock
    private ArchivedOrderRepository archivedOrderRepository;

    @InjectMocks
    private ReportGeneratorImpl reportGenerator;


    private final Long testUserId = 3L;
    private final List<String> currencyPairs = Arrays.asList("GBPUSD", "EURPLN", "USDCAD");


    private final OpenedPosition openedPosition1 = OpenedPosition.builder()
            .userId(testUserId)
            .currencyPair("GBPUSD")
            .amount(new BigDecimal("1208.15"))
            .longPosition(true)
            .openingPrice(new BigDecimal("1.1853"))
            .build();

    private final OpenedPosition openedPosition2 = OpenedPosition.builder()
            .userId(testUserId)
            .currencyPair("USDCAD")
            .amount(new BigDecimal("3580.90"))
            .longPosition(false)
            .openingPrice(new BigDecimal("1.2373"))
            .build();


    private final OpeningOrder openingOrder = OpeningOrder.builder()
            .userId(testUserId)
            .currencyPair("GBPUSD")
            .amount(new BigDecimal("8510.30"))
            .longPosition(false)
            .orderPrice(new BigDecimal("1.1853"))
            .triggeredAbove(true)
            .build();

    private final ClosingOrderForReportGeneration closingOrder = new ClosingOrderForReportGeneration();


    @BeforeEach
    void testSetUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void positionsReport() throws NoSuchFieldException, IllegalAccessException {
        // given
        setField(ForexPosition.class, "id", openedPosition1, 15L);
        setField(ForexPosition.class, "openingTimestamp", openedPosition1, new Timestamp(1222222222222L));

        setField(ForexPosition.class, "id", openedPosition2, 18L);
        setField(ForexPosition.class, "openingTimestamp", openedPosition2, new Timestamp(1333333333333L));

        final PositionsReportFilterDto filter = new PositionsReportFilterDto();
        filter.setCurrencyPairs(new HashSet<>(currencyPairs));
        filter.setOpeningDateBefore(LocalDate.of(2022, 2, 10));
        filter.setPositionAmountGreaterThan(new BigDecimal("1000.0"));
        filter.setLongPosition(true);

        Mockito.when(
                archivedPositionRepository.queryPositionsForReportGeneration(
                        testUserId, filter.getCurrencyPairs(), filter.getOpeningDateBefore(),
                        filter.getOpeningDateAfter(), filter.getClosingDateBefore(), filter.getClosingDateAfter()
                )
        ).thenReturn(Arrays.asList(openedPosition1, openedPosition2));

        // when
        String generatedReport = reportGenerator.positionsReport(testUserId, filter);

        // then
        Mockito.verify(archivedPositionRepository)
                .queryPositionsForReportGeneration(
                        testUserId, filter.getCurrencyPairs(), filter.getOpeningDateBefore(),
                        filter.getOpeningDateAfter(), filter.getClosingDateBefore(), filter.getClosingDateAfter()
                );

        Assertions.assertEquals(
                "ID,USER_ID,CURRENCY_PAIR,AMOUNT,OPENING_PRICE,LONG_POSITION," +
                        "OPENING_TIMESTAMP,STATUS,CLOSING_PRICE,PROFIT,CLOSING_TIMESTAMP\n" +
                        "15,3,GBPUSD,1208.15,1.1853,true,2008-09-24 04:10:22.222,opened,,,\n",
                generatedReport
        );
    }

    @Test
    void ordersReport() throws NoSuchFieldException, IllegalAccessException {
        // given
        setField(TransactionOrder.class, "id", openingOrder, 31L);
        setField(TransactionOrder.class, "orderTimestamp", openingOrder, new Timestamp(1222222222222L));

        setField(TransactionOrder.class, "id", closingOrder, 41L);
        setField(TransactionOrder.class, "orderTimestamp", closingOrder, new Timestamp(1444444444444L));
        setField(TransactionOrder.class, "userId", closingOrder, testUserId);
        setField(TransactionOrder.class, "orderPrice", closingOrder, new BigDecimal("4.6318"));
        setField(TransactionOrder.class, "triggeredAbove", closingOrder, false);
        setField(TransactionOrder.class, "orderStatus", closingOrder, OrderStatus.PENDING_ORDER);
        setField(ClosingOrderForReportGeneration.class, "currencyPair", closingOrder, "EURPLN");
        setField(ClosingOrderForReportGeneration.class, "amount", closingOrder, new BigDecimal("22315.90"));
        setField(ClosingOrderForReportGeneration.class, "longPosition", closingOrder, false);
        setField(ClosingOrderForReportGeneration.class, "forexPositionId", closingOrder, 50L);

        final OrdersReportFilterDto filter = new OrdersReportFilterDto();
        filter.setCurrencyPairs(new HashSet<>(currencyPairs));
        filter.setOrderStatuses(Collections.singleton(OrderStatus.PENDING_ORDER));
        filter.setOrderDateBefore(LocalDate.of(2022, 2, 10));
        filter.setAmountGreaterThan(new BigDecimal("2000.0"));
        filter.setLongPosition(false);

        Mockito.when(
                archivedOrderRepository.queryOrdersForReportGeneration(
                        testUserId, filter.getCurrencyPairs(), filter.getOrderStatuses(),
                        filter.getOrderDateBefore(), filter.getOrderDateAfter()
                )
        ).thenReturn(Arrays.asList(openingOrder, closingOrder));

        // when
        String generatedReport = reportGenerator.ordersReport(testUserId, filter);

        // then
        Mockito.verify(archivedOrderRepository).queryOrdersForReportGeneration(
                testUserId, filter.getCurrencyPairs(), filter.getOrderStatuses(),
                filter.getOrderDateBefore(), filter.getOrderDateAfter()
        );

        Assertions.assertEquals(
                "ID,USER_ID,CURRENCY_PAIR,AMOUNT,ORDER_PRICE,LONG_POSITION," +
                        "ORDER_TIMESTAMP,ORDER_STATUS,TRIGGERED_ABOVE,EXECUTION_TIMESTAMP," +
                        "ORDER_TYPE,POSITION_ID,OPENED_POSITION_ID\n" +
                        "31,3,GBPUSD,8510.30,1.1853,false,2008-09-24 04:10:22.222,P,true,,opening,,\n" +
                        "41,3,EURPLN,22315.90,4.6318,false,2015-10-10 04:34:04.444,P,false,,closing,50,\n",
                generatedReport
        );
    }
}
