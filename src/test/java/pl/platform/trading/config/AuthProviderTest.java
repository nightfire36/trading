package pl.platform.trading.config;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import pl.platform.trading.entity.User;
import pl.platform.trading.repository.UserRepository;
import testutils.PrivateFieldSetter;

import java.math.BigDecimal;
import java.util.Collections;

class AuthProviderTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private BasicConfig basicConfig;

    @InjectMocks
    private AuthProvider authProvider;

    private final Long testUserId = 3L;
    private final String password = "password12345";
    private final String secretSalt = "secretSalt";

    private final User testUser = User.builder()
            .firstName("ExampleFirstName")
            .lastName("ExampleLastName")
            .email("example_user@mail.com")
            .password("$2a$10$jAgsZGWRjXi3IQrA7NLIWu39xMMJ.rEVDW9rbrMw8M29S7x28qLle")
            .accountBalance(new BigDecimal("20500"))
            .build();


    @BeforeEach
    void testSetUp() {
        MockitoAnnotations.openMocks(this);

        Mockito.when(userRepository.findByEmail(testUser.getEmail()))
                .thenReturn(testUser);
        Mockito.when(basicConfig.getAdditionalSecretSalt())
                .thenReturn(secretSalt);
    }

    @Test
    void authenticate() throws NoSuchFieldException, IllegalAccessException {
        // given
        Authentication authenticationInput = new UsernamePasswordAuthenticationToken(testUser.getEmail(), password);
        PrivateFieldSetter.setField(User.class, "id", testUser, testUserId);

        // when
        Authentication authenticated = authProvider.authenticate(authenticationInput);

        // then
        Mockito.verify(userRepository)
                .findByEmail(testUser.getEmail());

        Assertions.assertEquals(testUserId, authenticated.getPrincipal());
        Assertions.assertEquals(password + "." + secretSalt, authenticated.getCredentials());
        Assertions.assertEquals(
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")),
                authenticated.getAuthorities()
        );
    }

    @Test
    void authenticateExceptionWhenBadPassword() {
        // given
        Authentication authenticationInput = new UsernamePasswordAuthenticationToken(
                testUser.getEmail(), "incorrectPassword"
        );

        // when
        Assertions.assertThrows(
                BadCredentialsException.class,
                () -> authProvider.authenticate(authenticationInput)
        );

        // then
        Mockito.verify(userRepository)
                .findByEmail(testUser.getEmail());
    }
}
