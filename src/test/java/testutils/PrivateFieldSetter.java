package testutils;

import java.lang.reflect.Field;

public class PrivateFieldSetter {
    public static void setField(
            Class<?> type, String fieldName, Object objectInWhichSet, Object valueToSet
    ) throws NoSuchFieldException, IllegalAccessException {
        Field fieldToSet = type.getDeclaredField(fieldName);
        fieldToSet.setAccessible(true);
        fieldToSet.set(objectInWhichSet, valueToSet);
    }
}
