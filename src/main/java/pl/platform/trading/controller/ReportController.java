package pl.platform.trading.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.platform.trading.controller.message.request.OrdersReportFilterDto;
import pl.platform.trading.controller.message.request.PositionsReportFilterDto;
import pl.platform.trading.report.ReportGenerator;

import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/report")
public class ReportController {
    private final ReportGenerator reportGenerator;

    @GetMapping(value = "positions_report", produces = "text/csv")
    public ResponseEntity<Resource> generatePositionsReport(
            Principal principal,
            PositionsReportFilterDto positionsReportFilterDto
    ) {
        String report = reportGenerator.positionsReport(
                Long.parseLong(principal.getName()),
                positionsReportFilterDto
        );
        return new ResponseEntity<>(
                convertStringToStreamResource(report),
                generateHttpHeadersForReport("positions_report"),
                HttpStatus.OK
        );

    }

    @GetMapping(value = "orders_report", produces = "text/csv")
    public ResponseEntity<Resource> generateOrdersReport(
            Principal principal,
            OrdersReportFilterDto ordersReportFilterDto
    ) {
        String report = reportGenerator.ordersReport(
                Long.parseLong(principal.getName()),
                ordersReportFilterDto
        );
        return new ResponseEntity<>(
                convertStringToStreamResource(report),
                generateHttpHeadersForReport("orders_report"),
                HttpStatus.OK
        );
    }

    private InputStreamResource convertStringToStreamResource(String report) {
        return new InputStreamResource(
                new ByteArrayInputStream(
                        report.getBytes(StandardCharsets.UTF_8)
                )
        );
    }

    private HttpHeaders generateHttpHeadersForReport(String filenamePrefix) {
        String filename = String.format(
                "%s_%s.csv",
                filenamePrefix,
                new SimpleDateFormat("dd-MM-yyyy")
                        .format(new Date())
        );

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
        return httpHeaders;
    }
}
