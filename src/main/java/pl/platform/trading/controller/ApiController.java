package pl.platform.trading.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import pl.platform.trading.controller.message.response.ResponseStatus;
import pl.platform.trading.controller.message.response.account.AccountInfo;
import pl.platform.trading.entity.ExchangeRate;
import pl.platform.trading.entity.order.OrderType;
import pl.platform.trading.entity.order.TransactionOrder;
import pl.platform.trading.entity.position.ForexPosition;
import pl.platform.trading.entity.position.PositionStatus;
import pl.platform.trading.resolver.PlatformModel;

import java.math.BigDecimal;
import java.security.Principal;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class ApiController {

    private final PlatformModel platformModel;


    @GetMapping("/rates")
    public List<ExchangeRate> getRates() {
        return platformModel.getRates();
    }

    @GetMapping("/rate")
    public ExchangeRate getPairRate(@RequestParam String pair) {
        return platformModel.getPairRate(pair);
    }

    @GetMapping("/account_info")
    public AccountInfo getAccountInfo(Principal principal) {
        return new AccountInfo(platformModel.userInfo(getUserId(principal)));
    }

    @GetMapping("/positions")
    public List<? extends ForexPosition> getPositions(
            Principal principal,
            @RequestParam(required = false) String positionType
    ) {
        Long currentUserId = getUserId(principal);
        if (positionType == null) {
            List<ForexPosition> positions = new LinkedList<>();
            positions.addAll(platformModel.getOpenedPositions(currentUserId));
            positions.addAll(platformModel.getClosedPositions(currentUserId));
            return positions;
        } else if(PositionStatus.OPENED_POSITION.equalsIgnoreCase(positionType)) {
            return platformModel.getOpenedPositions(currentUserId);
        } else if(PositionStatus.CLOSED_POSITION.equalsIgnoreCase(positionType)) {
            return platformModel.getClosedPositions(currentUserId);
        } else {
            return Collections.emptyList();
        }
    }

    @PostMapping("/open_position")
    public ResponseStatus openPosition(
            Principal principal,
            @RequestParam String pair,
            @RequestParam BigDecimal amount,
            @RequestParam Boolean longPosition
    ) {
        return new ResponseStatus(
                platformModel.openPosition(
                        getUserId(principal), pair, amount, longPosition
                )
        );
    }

    @GetMapping("/orders")
    public List<? extends TransactionOrder> getOrders(
            Principal principal,
            @RequestParam(required = false) String orderType,
            @RequestParam(required = false) Character orderStatus
    ) {
        Long currentUserId = getUserId(principal);
        if(orderType == null) {
            List<TransactionOrder> orders = new LinkedList<>();
            orders.addAll(platformModel.getOpeningOrders(currentUserId, orderStatus));
            orders.addAll(platformModel.getClosingOrders(currentUserId, orderStatus));
            return orders;
        } else if(OrderType.OPENING_ORDER.equalsIgnoreCase(orderType)) {
            return platformModel.getOpeningOrders(currentUserId, orderStatus);
        } else if(OrderType.CLOSING_ORDER.equalsIgnoreCase(orderType)) {
            return platformModel.getClosingOrders(currentUserId, orderStatus);
        } else {
            return Collections.emptyList();
        }
    }

    @PostMapping("/place_opening_order")
    public ResponseStatus placeOpeningOrder(
            Principal principal,
            @RequestParam String pair,
            @RequestParam BigDecimal amount,
            @RequestParam Boolean trigger,
            @RequestParam BigDecimal price,
            @RequestParam Boolean orderLong
    ) {
        return new ResponseStatus(
                platformModel.placeOpeningOrder(
                        getUserId(principal), pair, amount, trigger, price, orderLong
                )
        );
    }

    @PostMapping("/place_closing_order")
    public ResponseStatus placeClosingOrder(
            Principal principal,
            @RequestParam Long positionId,
            @RequestParam BigDecimal price,
            @RequestParam Boolean trigger
    ) {
        return new ResponseStatus(
                platformModel.placeClosingOrder(
                        getUserId(principal), positionId, trigger, price
                )
        );
    }

    @PostMapping("/close_position")
    public ResponseStatus closePosition(Principal principal, @RequestParam Long positionId) {
        return new ResponseStatus(platformModel.closePosition(getUserId(principal), positionId));
    }

    @PostMapping("/cancel_order")
    public ResponseStatus cancelOrder(Principal principal, @RequestParam Long orderId) {
        return new ResponseStatus(platformModel.cancelOrder(getUserId(principal), orderId));
    }

    private Long getUserId(Principal principal) {
        return Long.parseLong(principal.getName());
    }
}
