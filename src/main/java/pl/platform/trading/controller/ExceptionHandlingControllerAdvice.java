package pl.platform.trading.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.platform.trading.controller.message.response.ResponseStatus;
import pl.platform.trading.resolver.ModelStatusCode;

import java.util.NoSuchElementException;

@ControllerAdvice
public class ExceptionHandlingControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoSuchElementException.class)
    ResponseEntity<ResponseStatus> noSuchElementExceptionHandler() {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(new ResponseStatus(ModelStatusCode.ERROR_CANNOT_FIND_ELEMENT));
    }
}
