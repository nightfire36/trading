package pl.platform.trading.controller.message.response.register;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class RegisterResponse {
    private final Integer status;
    private final Integer invalidFields;
}
