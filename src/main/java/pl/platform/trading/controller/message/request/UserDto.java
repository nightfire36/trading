package pl.platform.trading.controller.message.request;

import lombok.Setter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.platform.trading.entity.User;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.math.BigDecimal;

@Setter
public class UserDto {

    @NotEmpty
    @Size(min = 3, max = 20)
    private String firstName;

    @NotEmpty
    @Size(min = 3, max = 20)
    private String lastName;

    @NotEmpty
    @Email
    @Size(min = 3, max = 30)
    private String email;

    @NotEmpty
    @Size(min = 6, max = 30)
    private String password;

    public User getUserFromDto(String additionalSecretSalt) {
        return User.builder()
                .firstName(this.firstName)
                .lastName(this.lastName)
                .email(this.email)
                .password(new BCryptPasswordEncoder().encode(this.password + "." + additionalSecretSalt))
                .accountBalance(new BigDecimal("0.0"))
                .status(0)
                .build();
    }
}
