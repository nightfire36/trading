package pl.platform.trading.controller.message.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
public class PositionsReportFilterDto {
    private Set<String> currencyPairs;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate openingDateAfter;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate openingDateBefore;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate closingDateAfter;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate closingDateBefore;

    private BigDecimal positionAmountGreaterThan;
    private BigDecimal positionAmountLowerThan;

    private BigDecimal openingPriceGreaterThan;
    private BigDecimal openingPriceLowerThan;

    private Boolean longPosition;
    private Boolean openedPosition;
}
