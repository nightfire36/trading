package pl.platform.trading.controller.message.response;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class ResponseStatus {
    private final int status;
}
