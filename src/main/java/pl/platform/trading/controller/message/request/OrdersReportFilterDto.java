package pl.platform.trading.controller.message.request;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
public class OrdersReportFilterDto {
    private Set<String> currencyPairs;
    private Set<Character> orderStatuses;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate orderDateAfter;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate orderDateBefore;

    private BigDecimal orderPriceGreaterThan;
    private BigDecimal orderPriceLowerThan;

    private BigDecimal amountGreaterThan;
    private BigDecimal amountLowerThan;

    private Boolean triggeredAbove;
    private Boolean longPosition;
    private Boolean openingOrder;
}
