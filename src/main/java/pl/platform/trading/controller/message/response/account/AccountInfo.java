package pl.platform.trading.controller.message.response.account;

import lombok.Getter;
import pl.platform.trading.entity.User;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Getter
public class AccountInfo {
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Timestamp createdAt;
    private final BigDecimal accountBalance;

    public AccountInfo(User user) {
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.createdAt = user.getCreatedAt();
        this.accountBalance = user.getAccountBalance();
    }
}
