package pl.platform.trading.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import pl.platform.trading.controller.message.request.UserDto;
import pl.platform.trading.controller.message.response.ResponseStatus;
import pl.platform.trading.controller.message.response.register.InvalidFieldCode;
import pl.platform.trading.controller.message.response.register.RegisterResponse;
import pl.platform.trading.resolver.ModelStatusCode;
import pl.platform.trading.resolver.PlatformModel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
public class UserController {

    private final PlatformModel platformModel;


    @GetMapping("/csrf_token")
    public String getCsrfToken(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        HttpSessionCsrfTokenRepository httpSessionCsrfTokenRepository = new HttpSessionCsrfTokenRepository();
        CsrfToken csrfToken = httpSessionCsrfTokenRepository.loadToken(httpServletRequest);

        if(csrfToken == null) {
            csrfToken = httpSessionCsrfTokenRepository.generateToken(httpServletRequest);
            httpSessionCsrfTokenRepository.saveToken(csrfToken, httpServletRequest, httpServletResponse);
        }

        return csrfToken.getToken();
    }

    @GetMapping("/login_check")
    public ResponseStatus loginCheck() {
        return new ResponseStatus(ModelStatusCode.STATUS_SUCCESS);
    }

    @PostMapping("/register")
    public RegisterResponse register(@RequestBody @Valid UserDto userDto, BindingResult result) {
        if(!result.hasErrors()) {
            return new RegisterResponse(platformModel.registerUser(userDto), 0);
        } else {
            int invalidFields = 0;
            List<FieldError> errorsList = result.getFieldErrors();
            for (FieldError error : errorsList) {
                // bitmask
                switch (error.getField()) {
                    case "firstName":
                        invalidFields |= InvalidFieldCode.INVALID_FIRST_NAME;
                        break;
                    case "lastName":
                        invalidFields |= InvalidFieldCode.INVALID_LAST_NAME;
                        break;
                    case "email":
                        invalidFields |= InvalidFieldCode.INVALID_EMAIL;
                        break;
                    case "password":
                        invalidFields |= InvalidFieldCode.INVALID_PASSWORD;
                        break;
                }
            }
            return new RegisterResponse(ModelStatusCode.ERROR_INVALID_FIELD_VALUE, invalidFields);
        }
    }

}
