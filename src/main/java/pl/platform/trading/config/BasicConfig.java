package pl.platform.trading.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "trading")
@EnableScheduling
public class BasicConfig {
    private String ratesProviderUrl;
    private String additionalSecretSalt;
    private Boolean shouldStoreReceivedRatesInDb;
}
