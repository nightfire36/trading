package pl.platform.trading.config.login;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import pl.platform.trading.resolver.ModelStatusCode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class LoginFailureHandler extends SimpleUrlAuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(
            HttpServletRequest request, HttpServletResponse response, AuthenticationException exception
    ) {
        log.error("Login failed. Exception: ", exception);

        // set response to failed
        LoginResponseWriter.writeResponse(
                response,
                HttpServletResponse.SC_UNAUTHORIZED,
                exception.getClass().equals(BadCredentialsException.class) ?
                        ModelStatusCode.ERROR_BAD_CREDENTIALS : ModelStatusCode.ERROR_UNKNOWN_LOGIN_ERROR
        );
    }
}
