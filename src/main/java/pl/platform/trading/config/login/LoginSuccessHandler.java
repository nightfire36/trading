package pl.platform.trading.config.login;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import pl.platform.trading.resolver.ModelStatusCode;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class LoginSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

    @Override
    public void onAuthenticationSuccess(
            HttpServletRequest request, HttpServletResponse response, Authentication authentication
    ) {
        log.info("Successful login of user with ID: {}", authentication.getName());
        clearAuthenticationAttributes(request);

        LoginResponseWriter.writeResponse(response, HttpServletResponse.SC_OK, ModelStatusCode.STATUS_SUCCESS);
    }
}
