package pl.platform.trading.config.login;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.http.server.ServletServerHttpResponse;
import pl.platform.trading.controller.message.response.ResponseStatus;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class LoginResponseWriter {
    public static void writeResponse(HttpServletResponse httpServletResponse, int httpCode, int responseCode) {
        httpServletResponse.setStatus(httpCode);

        try {
            new MappingJackson2HttpMessageConverter().write(
                    new ResponseStatus(responseCode),
                    ResponseStatus.class,
                    null,
                    new ServletServerHttpResponse(httpServletResponse)
            );
        } catch (IOException e) {
            log.error("Exception while writing login response: ", e);
        }
    }
}
