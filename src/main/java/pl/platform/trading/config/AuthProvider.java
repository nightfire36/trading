package pl.platform.trading.config;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.platform.trading.entity.User;
import pl.platform.trading.repository.UserRepository;

import java.util.Collections;

@RequiredArgsConstructor
@Component
public class AuthProvider implements AuthenticationProvider {

    private final UserRepository userRepository;
    private final BasicConfig basicConfig;

    @Override
    public Authentication authenticate(Authentication auth) throws BadCredentialsException {
        String username = auth.getName();
        String password = auth.getCredentials().toString();

        if(password != null) {
            password = password + "." + basicConfig.getAdditionalSecretSalt();
        }

        User user = userRepository.findByEmail(username);

        if(username == null || password == null || user == null) {
            throw new BadCredentialsException("Invalid username or password");
        }

        if (username.equals(user.getEmail()) && new BCryptPasswordEncoder().matches(password, user.getPassword())) {
            return new UsernamePasswordAuthenticationToken(
                    user.getId(),
                    password,
                    Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER"))
            );
        } else {
            throw new BadCredentialsException("Invalid username or password");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
