package pl.platform.trading.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "archiver")
public class ArchiverConfig {
    private String positionsCron;
    private String openingOrdersCron;

    private Long archivePositionsOlderThan;       // in days
    private Long archiveOpeningOrdersOlderThan;   // in days
}
