package pl.platform.trading.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import pl.platform.trading.config.login.LoginFailureHandler;
import pl.platform.trading.config.login.LoginSuccessHandler;

@RequiredArgsConstructor
@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final AuthProvider authProvider;

    @Override
    public void configure(HttpSecurity security) throws Exception {
        security
                .antMatcher("/**")
                .authorizeRequests()
                    .antMatchers("/", "/scripts/**", "/css/**", "/images/**", "/lib/**",
                            "/user/login", "/user/register", "/user/csrf_token")
                    .permitAll()
                .and()
                    .authorizeRequests()
                    .antMatchers("/**", "/api/**", "/user/**")
                    .fullyAuthenticated()
                .and()
                    .formLogin()
                    .loginProcessingUrl("/user/login")
                    .failureHandler(new LoginFailureHandler())
                    .successHandler(new LoginSuccessHandler())
                .and()
                    .logout()
                    .logoutUrl("/user/logout")
                    .logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler())
                .and()
                    .exceptionHandling()
                    .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED));
    }

    @Override
    public void configure(AuthenticationManagerBuilder authManager) throws Exception {
        authManager.authenticationProvider(authProvider);
    }
}
