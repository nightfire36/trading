package pl.platform.trading.repository.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.platform.trading.entity.order.ClosingOrder;
import pl.platform.trading.entity.order.OrderDiscriminatorValue;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Repository
public interface ClosingOrderRepository extends JpaRepository<ClosingOrder, Long> {
    List<ClosingOrder> findByUserId(Long userId);
    List<ClosingOrder> findByUserIdAndOrderStatus(Long userId, Character orderStatus);
    List<ClosingOrder> findByForexPositionId(Long positionId);

    @Query(
            nativeQuery = true,
            value = "select " +
                        "o.id, o.user_id, o.order_price, o.triggered_above, o.order_timestamp, " +
                        "o.order_status, o.dtype, o.forex_position_id, p.currency_pair, p.long_position, " +
                        "o.execution_timestamp " +
                    "from transaction_order as o " +
                    "inner join forex_position as p " +
                    "on o.forex_position_id = p.id " +
                    "where p.currency_pair = :currencyPair " +
                    "and o.dtype = '" + OrderDiscriminatorValue.CLOSING_ORDER + "' " +
                    "and o.order_status = :orderStatus " +
                    "and (" +
                    "(o.order_price < :ask and o.triggered_above = true and p.long_position = true) " +
                    "or (o.order_price < :bid and o.triggered_above = true and p.long_position = false) " +
                    "or (o.order_price > :ask and o.triggered_above = false and p.long_position = true) " +
                    "or (o.order_price > :bid and o.triggered_above = false and p.long_position = false)" +
                    ")"
    )
    List<ClosingOrder> findOrdersToExecute(String currencyPair, BigDecimal bid, BigDecimal ask, Character orderStatus);

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "update transaction_order " +
                    "set " +
                        "order_status = :orderStatus, " +
                        "execution_timestamp = NOW() " +
                    "where id = :orderId"
    )
    void markOrderAsExecuted(Long orderId, Character orderStatus);
}
