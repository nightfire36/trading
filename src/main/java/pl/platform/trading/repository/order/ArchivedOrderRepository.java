package pl.platform.trading.repository.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.platform.trading.entity.order.OrderDiscriminatorValue;
import pl.platform.trading.entity.order.TransactionOrder;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Repository
public interface ArchivedOrderRepository extends JpaRepository<TransactionOrder, Long> {

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "update transaction_order " +
                    "join forex_position " +
                    "on transaction_order.forex_position_id = forex_position.id " +
                    "set " +
                        "transaction_order.to_archive_flag = 'Y' " +
                    "where forex_position.to_archive_flag = 'Y'"
    )
    Integer markClosingOrdersAsArchived();

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "update transaction_order " +
                    "set " +
                        "to_archive_flag = 'Y' " +
                    "where DATE(order_timestamp) <= :olderThan " +
                    "and order_status <> :orderStatus"
    )
    Integer markOpeningOrdersAsArchived(LocalDate olderThan, Character orderStatus);

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "insert into archived_order " +
                    "select " +
                        "id, user_id, forex_position_id, currency_pair, amount, order_price, " +
                        "dtype, order_status, order_timestamp, long_position, triggered_above, " +
                        "opened_position_id, execution_timestamp " +
                    "from transaction_order " +
                    "where to_archive_flag = 'Y'"
    )
    Integer copyMarkedOrdersToArchive();

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "delete from transaction_order " +
                    "where to_archive_flag = 'Y'"
    )
    Integer deleteMarkedOrders();

    @Query(
            nativeQuery = true,
            value = "select " +
                        "id, user_id, forex_position_id, currency_pair, amount, order_price, dtype, " +
                        "order_status, order_timestamp, long_position, triggered_above, " +
                        "opened_position_id, execution_timestamp " +
                    "from transaction_order " +
                    "where user_id = :userId " +
                    "and dtype = '" + OrderDiscriminatorValue.OPENING_ORDER + "' " +
                    "and (currency_pair in :currencyPairs or COALESCE(:currencyPairs) IS NULL) " +
                    "and (order_status in :orderStatuses or COALESCE(:orderStatuses) IS NULL) " +
                    "and (DATE(order_timestamp) <= :orderDateBefore or :orderDateBefore IS NULL) " +
                    "and (DATE(order_timestamp) >= :orderDateAfter or :orderDateAfter IS NULL) " +
                    "union all " +
                    "select " +
                        "o.id, o.user_id, o.forex_position_id, p.currency_pair, p.amount, o.order_price, " +
                        "'" + OrderDiscriminatorValue.CLOSING_ORDER_FOR_REPORT_GENERATION + "' as dtype, " +
                        "o.order_status, o.order_timestamp, p.long_position, o.triggered_above, " +
                        "o.opened_position_id, o.execution_timestamp " +
                    "from transaction_order as o " +
                    "inner join forex_position as p " +
                    "on o.forex_position_id = p.id " +
                    "where o.user_id = :userId " +
                    "and o.dtype = '" + OrderDiscriminatorValue.CLOSING_ORDER + "' " +
                    "and (p.currency_pair in :currencyPairs or COALESCE(:currencyPairs) IS NULL) " +
                    "and (o.order_status in :orderStatuses or COALESCE(:orderStatuses) IS NULL) " +
                    "and (DATE(o.order_timestamp) <= :orderDateBefore or :orderDateBefore IS NULL) " +
                    "and (DATE(o.order_timestamp) >= :orderDateAfter or :orderDateAfter IS NULL) " +
                    "union all " +
                    "select " +
                        "id, user_id, archived_position_id as forex_position_id, currency_pair, amount, " +
                        "order_price, dtype, order_status, order_timestamp, long_position, triggered_above, " +
                        "opened_position_id, execution_timestamp " +
                    "from archived_order " +
                    "where user_id = :userId " +
                    "and dtype = '" + OrderDiscriminatorValue.OPENING_ORDER + "' " +
                    "and (currency_pair in :currencyPairs or COALESCE(:currencyPairs) IS NULL) " +
                    "and (order_status in :orderStatuses or COALESCE(:orderStatuses) IS NULL) " +
                    "and (DATE(order_timestamp) <= :orderDateBefore or :orderDateBefore IS NULL) " +
                    "and (DATE(order_timestamp) >= :orderDateAfter or :orderDateAfter IS NULL) " +
                    "union all " +
                    "select " +
                        "o.id, o.user_id, o.archived_position_id as forex_position_id, p.currency_pair, " +
                        "p.amount, o.order_price, o.order_status, o.order_timestamp, " +
                        "'" + OrderDiscriminatorValue.CLOSING_ORDER_FOR_REPORT_GENERATION + "' as dtype, " +
                        "p.long_position, o.triggered_above, o.opened_position_id, o.execution_timestamp " +
                    "from archived_order as o " +
                    "inner join archived_position as p " +
                    "on o.archived_position_id = p.id " +
                    "where o.user_id = :userId " +
                    "and o.dtype = '" + OrderDiscriminatorValue.CLOSING_ORDER + "' "+
                    "and (p.currency_pair in :currencyPairs or COALESCE(:currencyPairs) IS NULL) " +
                    "and (o.order_status in :orderStatuses or COALESCE(:orderStatuses) IS NULL) " +
                    "and (DATE(o.order_timestamp) <= :orderDateBefore or :orderDateBefore IS NULL) " +
                    "and (DATE(o.order_timestamp) >= :orderDateAfter or :orderDateAfter IS NULL) "
    )
    List<TransactionOrder> queryOrdersForReportGeneration(
            Long userId, Set<String> currencyPairs, Set<Character> orderStatuses,
            LocalDate orderDateBefore, LocalDate orderDateAfter
    );
}
