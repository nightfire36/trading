package pl.platform.trading.repository.order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.platform.trading.entity.order.OpeningOrder;
import pl.platform.trading.entity.order.OrderDiscriminatorValue;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Repository
public interface OpeningOrderRepository extends JpaRepository<OpeningOrder, Long> {
    List<OpeningOrder> findByUserId(Long userId);
    List<OpeningOrder> findByUserIdAndOrderStatus(Long userId, Character orderStatus);

    @Query(
            nativeQuery = true,
            value = "select " +
                        "id, user_id, currency_pair, amount, long_position, order_price, " +
                        "dtype, triggered_above, order_timestamp, order_status, " +
                        "opened_position_id, execution_timestamp " +
                    "from transaction_order " +
                    "where currency_pair = :currencyPair " +
                    "and dtype = '" + OrderDiscriminatorValue.OPENING_ORDER + "' " +
                    "and order_status = :orderStatus " +
                    "and (" +
                    "(order_price < :ask and triggered_above = true and long_position = true) " +
                    "or (order_price < :bid and triggered_above = true and long_position = false) " +
                    "or (order_price > :ask and triggered_above = false and long_position = true) " +
                    "or (order_price > :bid and triggered_above = false and long_position = false)" +
                    ")"
    )
    List<OpeningOrder> findOrdersToExecute(String currencyPair, BigDecimal bid, BigDecimal ask, Character orderStatus);

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "update transaction_order " +
                    "set " +
                        "order_status = :orderStatus, " +
                        "opened_position_id = :positionId, " +
                        "execution_timestamp = NOW() " +
                    "where id = :orderId"
    )
    void markOrderAsExecuted(Long orderId, Long positionId, Character orderStatus);
}
