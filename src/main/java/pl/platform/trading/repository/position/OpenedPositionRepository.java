package pl.platform.trading.repository.position;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.platform.trading.entity.position.OpenedPosition;
import pl.platform.trading.entity.position.PositionDiscriminatorValue;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

@Repository
public interface OpenedPositionRepository extends JpaRepository<OpenedPosition, Long> {
    List<OpenedPosition> findByUserId(Long userId);

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "update forex_position " +
                    "set " +
                        "dtype = '" + PositionDiscriminatorValue.CLOSED_POSITION + "', " +
                        "closing_price = :closingPrice, " +
                        "profit = :profit, " +
                        "closing_timestamp = NOW() " +
                    "where id = :id"
    )
    void closePosition(BigDecimal closingPrice, BigDecimal profit, Long id);
}
