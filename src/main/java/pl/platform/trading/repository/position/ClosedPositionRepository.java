package pl.platform.trading.repository.position;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.platform.trading.entity.position.ClosedPosition;

import java.util.List;

@Repository
public interface ClosedPositionRepository extends JpaRepository<ClosedPosition, Long> {
    List<ClosedPosition> findByUserId(Long userId);
}
