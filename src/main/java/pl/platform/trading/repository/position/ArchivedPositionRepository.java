package pl.platform.trading.repository.position;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.platform.trading.entity.position.ForexPosition;
import pl.platform.trading.entity.position.PositionDiscriminatorValue;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Repository
public interface ArchivedPositionRepository extends JpaRepository<ForexPosition, Long> {

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "update forex_position " +
                    "set " +
                        "to_archive_flag = 'Y' " +
                    "where DATE(closing_timestamp) <= :olderThan " +
                    "and dtype = '" + PositionDiscriminatorValue.CLOSED_POSITION + "'"
    )
    Integer markPositionsToBeArchived(LocalDate olderThan);

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "insert into archived_position " +
                    "select " +
                        "id, user_id, currency_pair, amount, opening_price, long_position, " +
                        "dtype, opening_timestamp, closing_price, profit, closing_timestamp " +
                    "from forex_position " +
                    "where to_archive_flag = 'Y'"
    )
    Integer copyMarkedPositionsToArchive();

    @Transactional
    @Modifying
    @Query(
            nativeQuery = true,
            value = "delete from forex_position " +
                    "where to_archive_flag = 'Y'"
    )
    Integer deleteMarkedPositions();

    @Query(
            nativeQuery = true,
            value = "select " +
                        "id, user_id, currency_pair, amount, opening_price, long_position, dtype, " +
                        "opening_timestamp, closing_price, profit, closing_timestamp " +
                    "from forex_position " +
                    "where user_id = :userId " +
                    "and (currency_pair in :currencyPairs or COALESCE(:currencyPairs) IS NULL) " +
                    "and (DATE(opening_timestamp) <= :openingDateBefore or :openingDateBefore IS NULL) " +
                    "and (DATE(opening_timestamp) >= :openingDateAfter or :openingDateAfter IS NULL) " +
                    "and (DATE(closing_timestamp) <= :closingDateBefore or :closingDateBefore IS NULL) " +
                    "and (DATE(closing_timestamp) >= :closingDateAfter or :closingDateAfter IS NULL) " +
                    "union all " +
                    "select " +
                        "id, user_id, currency_pair, amount, opening_price, long_position, dtype, " +
                        "opening_timestamp, closing_price, profit, closing_timestamp " +
                    "from archived_position " +
                    "where user_id = :userId " +
                    "and (currency_pair in :currencyPairs or COALESCE(:currencyPairs) IS NULL) " +
                    "and (DATE(opening_timestamp) <= :openingDateBefore or :openingDateBefore IS NULL) " +
                    "and (DATE(opening_timestamp) >= :openingDateAfter or :openingDateAfter IS NULL) " +
                    "and (DATE(closing_timestamp) <= :closingDateBefore or :closingDateBefore IS NULL) " +
                    "and (DATE(closing_timestamp) >= :closingDateAfter or :closingDateAfter IS NULL)"
    )
    List<ForexPosition> queryPositionsForReportGeneration(
            Long userId, Set<String> currencyPairs, LocalDate openingDateBefore, LocalDate openingDateAfter,
            LocalDate closingDateBefore, LocalDate closingDateAfter
    );
}
