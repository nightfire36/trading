package pl.platform.trading.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.sql.Timestamp;

@NoArgsConstructor
@Entity
public class ExchangeRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Getter
    private String currencyPair;
    @Getter
    private BigDecimal bid;
    @Getter
    private BigDecimal ask;
    @Getter
    private Timestamp timestamp;

    @Builder
    public ExchangeRate(String currencyPair, BigDecimal bid, BigDecimal ask, Timestamp timestamp) {
        this.currencyPair = currencyPair;
        this.bid = bid;
        this.ask = ask;
        this.timestamp = timestamp;
    }
}
