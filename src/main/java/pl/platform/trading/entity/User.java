package pl.platform.trading.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@NoArgsConstructor
@Getter
@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private Integer status;

    @Column(insertable = false, updatable = false)
    private Timestamp createdAt;

    @Setter
    private BigDecimal accountBalance;

    @Builder
    public User(String firstName, String lastName, String email,
                String password, BigDecimal accountBalance, Integer status) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.accountBalance = accountBalance;
        this.status = status;
    }
}
