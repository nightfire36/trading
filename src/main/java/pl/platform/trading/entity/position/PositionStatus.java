package pl.platform.trading.entity.position;

public class PositionStatus {
    public static final String OPENED_POSITION = "opened";
    public static final String CLOSED_POSITION = "closed";
}
