package pl.platform.trading.entity.position;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@NoArgsConstructor
@Getter
@Entity
@DiscriminatorValue(PositionDiscriminatorValue.CLOSED_POSITION)
public class ClosedPosition extends ForexPosition implements Serializable {
    private BigDecimal closingPrice;
    private BigDecimal profit;

    @Column(insertable = false, updatable = false)
    private Timestamp closingTimestamp;

    @Transient
    private final String positionStatus = PositionStatus.CLOSED_POSITION;
}
