package pl.platform.trading.entity.position;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

@NoArgsConstructor
@Getter
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.CHAR)
@DiscriminatorValue(PositionDiscriminatorValue.BASE_CLASS)
public abstract class ForexPosition implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;
    private String currencyPair;
    private BigDecimal amount;
    private BigDecimal openingPrice;
    private Boolean longPosition;

    @Column(insertable = false, updatable = false)
    private Timestamp openingTimestamp;

    public ForexPosition(Long userId, String currencyPair, BigDecimal amount,
                         BigDecimal openingPrice, Boolean longPosition) {
        this.userId = userId;
        this.currencyPair = currencyPair;
        this.amount = amount;
        this.openingPrice = openingPrice;
        this.longPosition = longPosition;
    }
}
