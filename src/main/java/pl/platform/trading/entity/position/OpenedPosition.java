package pl.platform.trading.entity.position;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Entity
@DiscriminatorValue(PositionDiscriminatorValue.OPENED_POSITION)
public class OpenedPosition extends ForexPosition implements Serializable {
    @Setter
    @Transient
    private BigDecimal currentProfit;

    @Transient
    private final String positionStatus = PositionStatus.OPENED_POSITION;

    @Builder
    public OpenedPosition(Long userId, String currencyPair, BigDecimal amount,
                          BigDecimal openingPrice, Boolean longPosition) {
        super(userId, currencyPair, amount, openingPrice, longPosition);
    }
}
