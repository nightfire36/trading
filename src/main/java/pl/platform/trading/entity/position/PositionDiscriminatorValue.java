package pl.platform.trading.entity.position;

public class PositionDiscriminatorValue {
    public static final String OPENED_POSITION = "O";
    public static final String CLOSED_POSITION = "C";
    public static final String BASE_CLASS = "null"; // reserved for abstract base class
}
