package pl.platform.trading.entity.order;

public class OrderType {
    public static final String OPENING_ORDER = "opening";
    public static final String CLOSING_ORDER = "closing";
}
