package pl.platform.trading.entity.order;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import pl.platform.trading.entity.position.ClosedPosition;
import pl.platform.trading.entity.position.OpenedPosition;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Entity
@DiscriminatorValue(OrderDiscriminatorValue.CLOSING_ORDER)
public class ClosingOrder extends TransactionOrder implements Serializable {
    private Long forexPositionId;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "forexPositionId", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private OpenedPosition openedPosition;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "forexPositionId", insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)
    private ClosedPosition closedPosition;

    @Transient
    private final String orderType = OrderType.CLOSING_ORDER;

    @Builder
    public ClosingOrder(Long userId, BigDecimal orderPrice, Boolean triggeredAbove, Long forexPositionId) {
        super(userId, orderPrice, triggeredAbove);
        this.forexPositionId = forexPositionId;
    }
}
