package pl.platform.trading.entity.order;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.platform.trading.report.OrderForReportGeneration;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Entity
@DiscriminatorValue(OrderDiscriminatorValue.OPENING_ORDER)
public class OpeningOrder extends TransactionOrder implements Serializable, OrderForReportGeneration {
    private String currencyPair;
    private BigDecimal amount;
    private Boolean longPosition;

    @Column(insertable = false, updatable = false)
    private Long openedPositionId;

    @Transient
    private final String orderType = OrderType.OPENING_ORDER;

    @Builder
    public OpeningOrder(Long userId, BigDecimal orderPrice, Boolean triggeredAbove,
                        String currencyPair, BigDecimal amount, Boolean longPosition) {
        super(userId, orderPrice, triggeredAbove);
        this.currencyPair = currencyPair;
        this.amount = amount;
        this.longPosition = longPosition;
    }
}
