package pl.platform.trading.entity.order;

public class OrderStatus {
    public static final Character PENDING_ORDER = 'P';
    public static final Character EXECUTED = 'E';
    public static final Character CANCELED = 'C';
    public static final Character SYSTEM_CANCELED = 'A';
}
