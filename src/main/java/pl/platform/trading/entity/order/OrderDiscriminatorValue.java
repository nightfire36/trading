package pl.platform.trading.entity.order;

public class OrderDiscriminatorValue {
    public static final String OPENING_ORDER = "O";
    public static final String CLOSING_ORDER = "C";
    public static final String CLOSING_ORDER_FOR_REPORT_GENERATION = "R";
    public static final String BASE_CLASS = "null"; // reserved for abstract base class
}
