package pl.platform.trading.entity.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;


@NoArgsConstructor
@Getter
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.CHAR)
@DiscriminatorValue(OrderDiscriminatorValue.BASE_CLASS)
public abstract class TransactionOrder implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Long userId;
    private BigDecimal orderPrice;
    private Boolean triggeredAbove;

    @Column(insertable = false, updatable = false)
    private Timestamp orderTimestamp;

    @Column(insertable = false, updatable = false)
    private Timestamp executionTimestamp;

    @Setter
    private Character orderStatus;

    public TransactionOrder(Long userId, BigDecimal orderPrice, Boolean triggeredAbove) {
        this.userId = userId;
        this.orderPrice = orderPrice;
        this.triggeredAbove = triggeredAbove;
        this.orderStatus = OrderStatus.PENDING_ORDER;
    }
}
