package pl.platform.trading.entity.order;

import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.platform.trading.report.OrderForReportGeneration;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;
import java.math.BigDecimal;

@NoArgsConstructor
@Getter
@Entity
@DiscriminatorValue(OrderDiscriminatorValue.CLOSING_ORDER_FOR_REPORT_GENERATION)
public class ClosingOrderForReportGeneration extends TransactionOrder implements Serializable, OrderForReportGeneration {
    private Long forexPositionId;

    private String currencyPair;
    private BigDecimal amount;
    private Boolean longPosition;
}
