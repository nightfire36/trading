package pl.platform.trading.report.archiver;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.platform.trading.config.ArchiverConfig;
import pl.platform.trading.entity.order.OrderStatus;
import pl.platform.trading.repository.order.ArchivedOrderRepository;
import pl.platform.trading.repository.position.ArchivedPositionRepository;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Slf4j
@Service
@RequiredArgsConstructor
public class Archiver {

    private final ArchiverConfig archiverConfig;

    private final ArchivedPositionRepository archivedPositionRepository;
    private final ArchivedOrderRepository archivedOrderRepository;

    @Scheduled(cron = "${archiver.positions-cron}")
    private void archivePositionsAndRelatedOrders() {
        try {
            LocalDate olderThanTimestamp = convertDaysToOlderThanDate(
                    archiverConfig.getArchivePositionsOlderThan()
            );

            archivedPositionRepository.markPositionsToBeArchived(olderThanTimestamp);
            archivedOrderRepository.markClosingOrdersAsArchived();

            archivedPositionRepository.copyMarkedPositionsToArchive();
            archivedOrderRepository.copyMarkedOrdersToArchive();

            archivedOrderRepository.deleteMarkedOrders();
            archivedPositionRepository.deleteMarkedPositions();

        } catch (Throwable t) {
            log.error("Exception during archiving positions and related orders. Exception details: ", t);
        }
    }

    @Scheduled(cron = "${archiver.opening-orders-cron}")
    private void archiveOpeningOrders() {
        try {
            LocalDate olderThanTimestamp = convertDaysToOlderThanDate(
                    archiverConfig.getArchiveOpeningOrdersOlderThan()
            );

            archivedOrderRepository.markOpeningOrdersAsArchived(olderThanTimestamp, OrderStatus.PENDING_ORDER);
            archivedOrderRepository.copyMarkedOrdersToArchive();
            archivedOrderRepository.deleteMarkedOrders();

        } catch (Throwable t) {
            log.error("Exception during archiving opening orders. Exception details: ", t);
        }
    }

    private LocalDate convertDaysToOlderThanDate(Long days) {
        return LocalDate.now().minus(
                days,
                ChronoUnit.DAYS
        );
    }
}
