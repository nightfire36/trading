package pl.platform.trading.report;

import java.math.BigDecimal;
import java.sql.Timestamp;

public interface OrderForReportGeneration {
    Long getId();
    Long getUserId();
    String getCurrencyPair();
    BigDecimal getAmount();
    BigDecimal getOrderPrice();
    Boolean getLongPosition();
    Timestamp getOrderTimestamp();
    Character getOrderStatus();
    Boolean getTriggeredAbove();
    Timestamp getExecutionTimestamp();
}
