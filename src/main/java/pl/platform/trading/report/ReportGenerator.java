package pl.platform.trading.report;

import pl.platform.trading.controller.message.request.OrdersReportFilterDto;
import pl.platform.trading.controller.message.request.PositionsReportFilterDto;

public interface ReportGenerator {
    String positionsReport(Long userId, PositionsReportFilterDto positionsReportFilterDto);
    String ordersReport(Long userId, OrdersReportFilterDto ordersReportFilterDto);
}
