package pl.platform.trading.report;

public class ReportCsvConstant {
    public static final String CSV_DELIMITER = ",";
    public static final String CSV_NEW_LINE_CHARACTER = "\n";
}
