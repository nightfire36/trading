package pl.platform.trading.report;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.platform.trading.controller.message.request.OrdersReportFilterDto;
import pl.platform.trading.controller.message.request.PositionsReportFilterDto;
import pl.platform.trading.entity.order.ClosingOrderForReportGeneration;
import pl.platform.trading.entity.order.OpeningOrder;
import pl.platform.trading.entity.order.OrderType;
import pl.platform.trading.entity.order.TransactionOrder;
import pl.platform.trading.entity.position.ClosedPosition;
import pl.platform.trading.entity.position.ForexPosition;
import pl.platform.trading.entity.position.OpenedPosition;
import pl.platform.trading.entity.position.PositionStatus;
import pl.platform.trading.repository.order.ArchivedOrderRepository;
import pl.platform.trading.repository.position.ArchivedPositionRepository;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class ReportGeneratorImpl implements ReportGenerator {

    private final ArchivedPositionRepository archivedPositionRepository;
    private final ArchivedOrderRepository archivedOrderRepository;

    @Override
    public String positionsReport(Long userId, PositionsReportFilterDto filter) {
        Set<String> nullSafeCurrencyPairs = Optional.ofNullable(filter.getCurrencyPairs())
                .orElse(Collections.emptySet());
        List<ForexPosition> queriedPositions = archivedPositionRepository.queryPositionsForReportGeneration(
                userId, nullSafeCurrencyPairs, filter.getOpeningDateBefore(), filter.getOpeningDateAfter(),
                filter.getClosingDateBefore(), filter.getClosingDateAfter()
        );
        List<ForexPosition> filteredPositions = filterPositions(filter, queriedPositions);
        return generatePositionsCsv(filteredPositions);
    }

    @Override
    public String ordersReport(Long userId, OrdersReportFilterDto filter) {
        Set<String> nullSafeCurrencyPairs = Optional.ofNullable(filter.getCurrencyPairs())
                .orElse(Collections.emptySet());
        Set<Character> nullSafeOrderStatuses = Optional.ofNullable(filter.getOrderStatuses())
                .orElse(Collections.emptySet());
        List<TransactionOrder> queriedOrders = archivedOrderRepository.queryOrdersForReportGeneration(
                userId, nullSafeCurrencyPairs, nullSafeOrderStatuses, filter.getOrderDateBefore(),
                filter.getOrderDateAfter()
        );
        List<TransactionOrder> filteredOrders = filterOrders(filter, queriedOrders);
        return generateOrdersCsv(filteredOrders);
    }

    private String generatePositionsCsv(List<ForexPosition> positions) {
        final String csvHeader = buildCsvRow(
                11, ReportCsvConstant.CSV_DELIMITER, ReportCsvConstant.CSV_NEW_LINE_CHARACTER,
                "ID", "USER_ID", "CURRENCY_PAIR", "AMOUNT", "OPENING_PRICE", "LONG_POSITION",
                "OPENING_TIMESTAMP", "STATUS", "CLOSING_PRICE", "PROFIT", "CLOSING_TIMESTAMP"
        );

        return csvHeader + positions.stream()
                .map(position ->
                        buildCsvRow(
                                11,
                                ReportCsvConstant.CSV_DELIMITER,
                                ReportCsvConstant.CSV_NEW_LINE_CHARACTER,
                                position.getId(),
                                position.getUserId(),
                                position.getCurrencyPair(),
                                position.getAmount(),
                                position.getOpeningPrice(),
                                position.getLongPosition(),
                                position.getOpeningTimestamp(),
                                position instanceof ClosedPosition ?
                                        PositionStatus.CLOSED_POSITION : PositionStatus.OPENED_POSITION,
                                position instanceof ClosedPosition ?
                                        ((ClosedPosition) position).getClosingPrice() : null,
                                position instanceof ClosedPosition ?
                                        ((ClosedPosition) position).getProfit() : null,
                                position instanceof ClosedPosition ?
                                        ((ClosedPosition) position).getClosingTimestamp() : null
                        )
                )
                .collect(Collectors.joining());
    }

    private List<ForexPosition> filterPositions(
            PositionsReportFilterDto filter, List<ForexPosition> positionsToFilter
    ) {
        return positionsToFilter.stream()
                .filter(e -> isFirstGreater(filter.getPositionAmountLowerThan(), e.getAmount()))
                .filter(e -> isSecondGreater(filter.getPositionAmountGreaterThan(), e.getAmount()))
                .filter(e -> isFirstGreater(filter.getOpeningPriceLowerThan(), e.getOpeningPrice()))
                .filter(e -> isSecondGreater(filter.getOpeningPriceGreaterThan(), e.getOpeningPrice()))
                .filter(e -> areBooleansEqual(filter.getLongPosition(), e.getLongPosition()))
                .filter(e ->
                        isInstanceOfCheck(filter.getOpenedPosition(), e, OpenedPosition.class, ClosedPosition.class))
                .collect(Collectors.toList());
    }

    private String generateOrdersCsv(List<TransactionOrder> orders) {
        final String csvHeader = buildCsvRow(
                13, ReportCsvConstant.CSV_DELIMITER, ReportCsvConstant.CSV_NEW_LINE_CHARACTER,
                "ID", "USER_ID", "CURRENCY_PAIR", "AMOUNT", "ORDER_PRICE", "LONG_POSITION",
                "ORDER_TIMESTAMP", "ORDER_STATUS", "TRIGGERED_ABOVE", "EXECUTION_TIMESTAMP", "ORDER_TYPE",
                "POSITION_ID", "OPENED_POSITION_ID"
        );

        return csvHeader + orders.stream()
                .map(e -> (OrderForReportGeneration) e)
                .map(order ->
                        buildCsvRow(
                                13,
                                ReportCsvConstant.CSV_DELIMITER,
                                ReportCsvConstant.CSV_NEW_LINE_CHARACTER,
                                order.getId(),
                                order.getUserId(),
                                order.getCurrencyPair(),
                                order.getAmount(),
                                order.getOrderPrice(),
                                order.getLongPosition(),
                                order.getOrderTimestamp(),
                                order.getOrderStatus(),
                                order.getTriggeredAbove(),
                                order.getExecutionTimestamp(),
                                order instanceof ClosingOrderForReportGeneration ?
                                        OrderType.CLOSING_ORDER : OrderType.OPENING_ORDER,
                                order instanceof ClosingOrderForReportGeneration ?
                                        ((ClosingOrderForReportGeneration) order).getForexPositionId() : null,
                                order instanceof OpeningOrder ?
                                        ((OpeningOrder) order).getOpenedPositionId() : null
                        )
                )
                .collect(Collectors.joining());
    }

    private List<TransactionOrder> filterOrders(OrdersReportFilterDto filter, List<TransactionOrder> ordersToFilter) {
        return ordersToFilter.stream()
                .filter(e -> isFirstGreater(filter.getOrderPriceLowerThan(), e.getOrderPrice()))
                .filter(e -> isSecondGreater(filter.getOrderPriceGreaterThan(), e.getOrderPrice()))
                .filter(e -> areBooleansEqual(filter.getTriggeredAbove(), e.getTriggeredAbove()))
                .filter(e -> isInstanceOfCheck(
                        filter.getOpeningOrder(), e, OpeningOrder.class, ClosingOrderForReportGeneration.class
                ))
                .filter(e ->
                        areBooleansEqual(filter.getLongPosition(), ((OrderForReportGeneration) e).getLongPosition()))
                .filter(e -> isFirstGreater(filter.getAmountLowerThan(), ((OrderForReportGeneration) e).getAmount()))
                .filter(e -> isSecondGreater(filter.getAmountGreaterThan(), ((OrderForReportGeneration) e).getAmount()))
                .collect(Collectors.toList());
    }

    private String buildCsvRow(int rowNumber, String delimiter, String newLineChar, Object... values) {
        if(values.length != rowNumber) {
            throw new IllegalArgumentException();
        }

        return Arrays.stream(values)
                .map(e -> Objects.isNull(e) ? "" : e.toString())
                .collect(Collectors.joining(delimiter)) + newLineChar;
    }

    private boolean isFirstGreater(BigDecimal firstNumber, BigDecimal secondNumber) {
        return Objects.isNull(firstNumber) || firstNumber.compareTo(secondNumber) > 0;
    }

    private boolean isSecondGreater(BigDecimal firstNumber, BigDecimal secondNumber) {
        return Objects.isNull(firstNumber) || firstNumber.compareTo(secondNumber) < 0;
    }

    private boolean areBooleansEqual(Boolean firstBoolean, Boolean secondBoolean) {
        return Objects.isNull(firstBoolean) || firstBoolean.equals(secondBoolean);
    }

    private boolean isInstanceOfCheck(Boolean indicator, Object obj, Class<?> firstClass, Class<?> secondClass) {
        return Objects.isNull(indicator) ||
                (indicator && firstClass.isInstance(obj)) ||
                (!indicator && secondClass.isInstance(obj));
    }
}
