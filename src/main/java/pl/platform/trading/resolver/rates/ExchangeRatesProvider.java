package pl.platform.trading.resolver.rates;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pl.platform.trading.config.BasicConfig;
import pl.platform.trading.entity.ExchangeRate;
import pl.platform.trading.repository.ExchangeRateRepository;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ExchangeRatesProvider {

    private final List<String> supportedCurrencyPairs = Arrays.asList(
            "EURUSD", "GBPUSD", "EURPLN", "USDPLN", "USDCAD",
            "USDCHF", "AUDUSD", "USDJPY", "EURJPY", "EURGBP"
    );

    @Getter
    private final List<ExchangeRate> rates;
    private final RestTemplate restTemplate;

    private final BasicConfig basicConfig;
    private final ExchangeRateRepository exchangeRateRepository;


    public ExchangeRatesProvider(
            BasicConfig basicConfig,
            ExchangeRateRepository exchangeRateRepository
    ) {
        this.basicConfig = basicConfig;
        this.exchangeRateRepository = exchangeRateRepository;
        this.rates = new ArrayList<>();
        this.restTemplate = new RestTemplate();
    }

    public void updateRates() {
        ResponseEntity<List<ExchangeRateDto>> ratesResponseEntity = restTemplate.exchange(
                basicConfig.getRatesProviderUrl(),
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<ExchangeRateDto>>() {}
        );

        if(ratesResponseEntity.getStatusCode() == HttpStatus.OK && ratesResponseEntity.getBody() != null) {
            List<ExchangeRate> receivedRates = transformRates(ratesResponseEntity.getBody());

            if(basicConfig.getShouldStoreReceivedRatesInDb()) {
                exchangeRateRepository.saveAll(receivedRates);
            }

            rates.clear();
            rates.addAll(receivedRates);
        } else {
            log.error("Could not receive exchange rates.");
            log.error("Rates provider returned HTTP status code: {}", ratesResponseEntity.getStatusCode());
        }
    }

    private List<ExchangeRate> transformRates(List<ExchangeRateDto> exchangeRateDtoList) {
        return exchangeRateDtoList.stream()
                .filter(e -> e.getCurrencyPair() != null)
                .filter(e -> supportedCurrencyPairs.contains(e.getCurrencyPair().toUpperCase()))
                .map(ExchangeRateDto::getExchangeRateFromDto)
                .collect(Collectors.toList());
    }

    public ExchangeRate getPairRate(String pair) {
        return rates.stream()
                .filter(rate -> rate.getCurrencyPair().equals(pair))
                .findFirst()
                .orElse(null);
    }
}
