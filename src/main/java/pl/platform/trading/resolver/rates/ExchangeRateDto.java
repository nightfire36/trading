package pl.platform.trading.resolver.rates;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import pl.platform.trading.entity.ExchangeRate;

import java.math.BigDecimal;
import java.sql.Timestamp;

public class ExchangeRateDto {

	@Getter
	@JsonProperty("Name")
	private String currencyPair;

	@JsonProperty("Bid")
	private BigDecimal bid;

	@JsonProperty("Ask")
	private BigDecimal ask;

	@JsonProperty("Min")
	private BigDecimal min;

	@JsonProperty("Max")
	private BigDecimal max;

	@JsonProperty("Time")
	private Timestamp timestamp;

	@JsonProperty("ChartDirection")
	private int chartDirection;

	public ExchangeRate getExchangeRateFromDto() {
		return ExchangeRate.builder()
				.currencyPair(currencyPair)
				.bid(bid)
				.ask(ask)
				.timestamp(timestamp)
				.build();
	}
}
