package pl.platform.trading.resolver;

import pl.platform.trading.entity.position.OpenedPosition;

import java.math.BigDecimal;

public interface TransactionResolver {
    void updateAccountBalance(Long userId, BigDecimal updateAmount);
    boolean closePosition(Long positionId);
    OpenedPosition openPosition(Long userId, String currencyPair, BigDecimal amount, Boolean longPosition);
    void resolve();
}
