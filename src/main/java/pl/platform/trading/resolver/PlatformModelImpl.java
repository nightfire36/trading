package pl.platform.trading.resolver;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import pl.platform.trading.config.BasicConfig;
import pl.platform.trading.controller.message.request.UserDto;
import pl.platform.trading.entity.ExchangeRate;
import pl.platform.trading.entity.User;
import pl.platform.trading.entity.order.ClosingOrder;
import pl.platform.trading.entity.order.OpeningOrder;
import pl.platform.trading.entity.order.OrderStatus;
import pl.platform.trading.entity.order.TransactionOrder;
import pl.platform.trading.entity.position.ClosedPosition;
import pl.platform.trading.entity.position.OpenedPosition;
import pl.platform.trading.repository.UserRepository;
import pl.platform.trading.repository.order.ClosingOrderRepository;
import pl.platform.trading.repository.order.OpeningOrderRepository;
import pl.platform.trading.repository.position.ClosedPositionRepository;
import pl.platform.trading.repository.position.OpenedPositionRepository;
import pl.platform.trading.resolver.rates.ExchangeRatesProvider;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class PlatformModelImpl implements PlatformModel {

    private final UserRepository userRepository;
    private final OpeningOrderRepository openingOrderRepository;
    private final ClosingOrderRepository closingOrderRepository;
    private final OpenedPositionRepository openedPositionRepository;
    private final ClosedPositionRepository closedPositionRepository;

    private final TransactionResolver resolver;
    private final ExchangeRatesProvider rates;
    private final BasicConfig basicConfig;


    @Override
    public Integer registerUser(UserDto userDto) {
        User user = userDto.getUserFromDto(basicConfig.getAdditionalSecretSalt());
        user.setAccountBalance(new BigDecimal("100000.0"));
        try {
            userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            return ModelStatusCode.ERROR_EMAIL_ALREADY_EXISTS;
        } catch (ConstraintViolationException e) {
            return ModelStatusCode.ERROR_CONSTRAINT_VIOLATION;
        } catch (Exception e) {
            return ModelStatusCode.ERROR_DB_SAVE_EXCEPTION;
        }
        return ModelStatusCode.STATUS_SUCCESS;
    }

    @Override
    public User userInfo(Long userId) {
        return userRepository
                .findById(userId)
                .orElseThrow(IllegalStateException::new);
    }

    @Override
    public List<ExchangeRate> getRates() {
        return rates.getRates();
    }

    @Override
    public ExchangeRate getPairRate(String pair) {
        return rates.getPairRate(pair);
    }

    @Override
    public List<OpenedPosition> getOpenedPositions(Long userId) {
        List<OpenedPosition> openedPositionList = Optional
                .ofNullable(openedPositionRepository.findByUserId(userId))
                .orElse(Collections.emptyList());

        return openedPositionList.stream()
                .peek(openedPosition ->
                        openedPosition.setCurrentProfit(
                                ProfitCalculator.calculateProfit(
                                        openedPosition,
                                        rates.getPairRate(openedPosition.getCurrencyPair())
                                )
                        )
                )
                .collect(Collectors.toList());
    }

    @Override
    public List<ClosedPosition> getClosedPositions(Long userId) {
        return closedPositionRepository.findByUserId(userId);
    }

    @Override
    public Integer openPosition(Long userId, String pair, BigDecimal amount, Boolean longPosition) {
        return resolver.openPosition(userId, pair, amount, longPosition) != null ?
                ModelStatusCode.STATUS_SUCCESS : ModelStatusCode.ERROR_NOT_ENOUGH_MONEY;
    }

    @Override
    public Integer closePosition(Long userId, Long positionId) {
        OpenedPosition opened = openedPositionRepository
                .findById(positionId)
                .orElseThrow(NoSuchElementException::new);

        if (userId.equals(opened.getUserId())) {
            resolver.closePosition(positionId);
            return ModelStatusCode.STATUS_SUCCESS;
        } else {
            return ModelStatusCode.ERROR_BELONGS_TO_ANOTHER_USER;
        }
    }

    @Override
    public List<OpeningOrder> getOpeningOrders(Long userId, Character orderStatus) {
        return orderStatus != null ?
                openingOrderRepository.findByUserIdAndOrderStatus(userId, orderStatus) :
                openingOrderRepository.findByUserId(userId);
    }

    @Override
    public List<ClosingOrder> getClosingOrders(Long userId, Character orderStatus) {
        return orderStatus != null ?
                closingOrderRepository.findByUserIdAndOrderStatus(userId, orderStatus) :
                closingOrderRepository.findByUserId(userId);
    }

    @Override
    public Integer placeOpeningOrder(
            Long userId, String pair, BigDecimal amount, Boolean trigger,
            BigDecimal price, Boolean longPosition
    ) {
        OpeningOrder order = OpeningOrder.builder()
                .userId(userId)
                .currencyPair(pair)
                .amount(amount)
                .orderPrice(price)
                .longPosition(longPosition)
                .triggeredAbove(trigger)
                .build();

        try {
            openingOrderRepository.save(order);
        } catch (Exception e) {
            log.error("Exception during saving placed order: ", e);
            return ModelStatusCode.ERROR_DB_SAVE_EXCEPTION;
        }

        resolver.resolve();

        return ModelStatusCode.STATUS_SUCCESS;
    }

    @Override
    public Integer placeClosingOrder(Long userId, Long positionId, Boolean trigger, BigDecimal price) {
        OpenedPosition opened = openedPositionRepository
                .findById(positionId)
                .orElseThrow(NoSuchElementException::new);

        if (userId.equals(opened.getUserId())) {

            ClosingOrder order = ClosingOrder.builder()
                    .userId(opened.getUserId())
                    .forexPositionId(positionId)
                    .orderPrice(price)
                    .triggeredAbove(trigger)
                    .build();

            try {
                closingOrderRepository.save(order);
            } catch (Exception e) {
                log.error("Exception during saving order for closure: ", e);
                return ModelStatusCode.ERROR_DB_SAVE_EXCEPTION;
            }
            resolver.resolve();

            return ModelStatusCode.STATUS_SUCCESS;

        } else {
            return ModelStatusCode.ERROR_BELONGS_TO_ANOTHER_USER;
        }
    }

    @Transactional
    @Override
    public Integer cancelOrder(Long userId, Long orderId) {
        TransactionOrder pending = openingOrderRepository
                .findById(orderId)
                .orElse(null);

        if(pending == null) {
            pending = closingOrderRepository
                    .findById(orderId)
                    .orElse(null);
        }

        if (pending != null) {
            if (userId.equals(pending.getUserId())) {
                try {
                    pending.setOrderStatus(OrderStatus.CANCELED);
                } catch (Exception e) {
                    log.error("Exception during canceling order: ", e);
                    return ModelStatusCode.ERROR_DB_SET_ORDER_STATUS_EXCEPTION;
                }
                return ModelStatusCode.STATUS_SUCCESS;
            } else {
                return ModelStatusCode.ERROR_BELONGS_TO_ANOTHER_USER;
            }
        } else {
            throw new NoSuchElementException("Could not find order with ID " + orderId);
        }
    }
}
