package pl.platform.trading.resolver;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import pl.platform.trading.entity.ExchangeRate;
import pl.platform.trading.entity.User;
import pl.platform.trading.entity.order.ClosingOrder;
import pl.platform.trading.entity.order.OpeningOrder;
import pl.platform.trading.entity.order.OrderStatus;
import pl.platform.trading.entity.position.OpenedPosition;
import pl.platform.trading.repository.UserRepository;
import pl.platform.trading.repository.order.ClosingOrderRepository;
import pl.platform.trading.repository.order.OpeningOrderRepository;
import pl.platform.trading.repository.position.OpenedPositionRepository;
import pl.platform.trading.resolver.rates.ExchangeRatesProvider;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;
import java.util.NoSuchElementException;

@Slf4j
@RequiredArgsConstructor
@Component
public class TransactionResolverImpl implements TransactionResolver {

    private final OpeningOrderRepository openingOrderRepository;
    private final ClosingOrderRepository closingOrderRepository;
    private final OpenedPositionRepository openedPositionRepository;
    private final UserRepository userRepository;
    private final ExchangeRatesProvider rates;

    @Transactional
    public void updateAccountBalance(Long userId, BigDecimal updateAmount) {
        User user = userRepository.findById(userId)
                .orElseThrow(IllegalStateException::new);

        BigDecimal accountBalance = user.getAccountBalance().add(updateAmount);
        user.setAccountBalance(accountBalance);
    }

    @Transactional
    public boolean closePosition(Long positionId) {
        OpenedPosition opened = openedPositionRepository
                .findById(positionId)
                .orElseThrow(NoSuchElementException::new);

        ExchangeRate exchangeRate = rates.getPairRate(opened.getCurrencyPair());
        BigDecimal transactionProfit = ProfitCalculator.calculateProfit(opened, exchangeRate);

        log.info("closing position: {}, {}, {}", opened.getLongPosition(), transactionProfit, opened.getId());

        try {
            closingOrderRepository.findByForexPositionId(positionId)
                    .stream()
                    .filter(e -> e.getOrderStatus() == OrderStatus.PENDING_ORDER)
                    .forEach(e -> e.setOrderStatus(OrderStatus.SYSTEM_CANCELED));

            openedPositionRepository.closePosition(
                    opened.getLongPosition() ?
                            exchangeRate.getBid() :
                            exchangeRate.getAsk(),
                    transactionProfit,
                    opened.getId()
            );

            // update user account balance
            updateAccountBalance(opened.getUserId(), opened.getAmount().add(transactionProfit));

        } catch (Exception e) {
            log.error("Exception thrown during closing position: ", e);
            return false;
        }
        return true;
    }

    @Transactional
    public OpenedPosition openPosition(Long userId, String currencyPair, BigDecimal amount, Boolean longPosition) {
        User user = userRepository
                .findById(userId)
                .orElseThrow(IllegalStateException::new);

        if (user.getAccountBalance().compareTo(amount) >= 0) {

            OpenedPosition openedPosition = openedPositionRepository.save(
                    OpenedPosition.builder()
                            .userId(userId)
                            .currencyPair(currencyPair)
                            .amount(amount)
                            .longPosition(longPosition)
                            .openingPrice(
                                    longPosition ?
                                            rates.getPairRate(currencyPair).getAsk() :
                                            rates.getPairRate(currencyPair).getBid()
                            )
                            .build()
            );

            // update user amount of money
            updateAccountBalance(userId, amount.negate());

            return openedPosition;
        }
        return null;
    }

    private void executeOpeningOrders(List<OpeningOrder> openingOrders) {
        openingOrders.forEach(
                openingOrder -> {
                    try {
                        OpenedPosition openedPosition = openPosition(
                                openingOrder.getUserId(),
                                openingOrder.getCurrencyPair(),
                                openingOrder.getAmount(),
                                openingOrder.getLongPosition()
                        );
                        openingOrderRepository.markOrderAsExecuted(
                                openingOrder.getId(), openedPosition.getId(), OrderStatus.EXECUTED
                        );
                    } catch (Exception e) {
                        log.error("Exception thrown during executing Opening orders: ", e);
                    }
                }
        );
    }

    private void executeClosingOrders(List<ClosingOrder> closingOrders) {
        closingOrders.forEach(
                closingOrder -> {
                    try {
                        closePosition(closingOrder.getForexPositionId());
                        closingOrderRepository.markOrderAsExecuted(closingOrder.getId(), OrderStatus.EXECUTED);
                    } catch (Exception e) {
                        log.error("Exception thrown during executing Closing orders: ", e);
                    }
                }
        );
    }

    public void resolve() {
        rates.getRates().forEach(
                rate -> {
                    log.debug("Resolving: currency pair={}, bid={}, ask={}", rate.getCurrencyPair(),
                            rate.getBid(), rate.getAsk());

                    executeOpeningOrders(
                            openingOrderRepository.findOrdersToExecute(
                                    rate.getCurrencyPair(),
                                    rate.getBid(),
                                    rate.getAsk(),
                                    OrderStatus.PENDING_ORDER
                            )
                    );
                    executeClosingOrders(
                            closingOrderRepository.findOrdersToExecute(
                                    rate.getCurrencyPair(),
                                    rate.getBid(),
                                    rate.getAsk(),
                                    OrderStatus.PENDING_ORDER
                            )
                    );
                }
        );
    }
}
