package pl.platform.trading.resolver;

import pl.platform.trading.controller.message.request.UserDto;
import pl.platform.trading.entity.ExchangeRate;
import pl.platform.trading.entity.User;
import pl.platform.trading.entity.order.ClosingOrder;
import pl.platform.trading.entity.order.OpeningOrder;
import pl.platform.trading.entity.position.ClosedPosition;
import pl.platform.trading.entity.position.OpenedPosition;

import java.math.BigDecimal;
import java.util.List;

public interface PlatformModel {
    Integer registerUser(UserDto userDto);
    User userInfo(Long userId);
    List<ExchangeRate> getRates();
    ExchangeRate getPairRate(String pair);
    List<OpenedPosition> getOpenedPositions(Long userId);
    List<ClosedPosition> getClosedPositions(Long userId);
    Integer openPosition(Long userId, String pair, BigDecimal amount, Boolean longPosition);
    Integer closePosition(Long userId, Long positionId);
    List<OpeningOrder> getOpeningOrders(Long userId, Character orderStatus);
    List<ClosingOrder> getClosingOrders(Long userId, Character orderStatus);
    Integer placeOpeningOrder(Long userId, String pair, BigDecimal amount, Boolean trigger,
                              BigDecimal price, Boolean longPosition);
    Integer placeClosingOrder(Long userId, Long positionId, Boolean trigger, BigDecimal price);
    Integer cancelOrder(Long userId, Long orderId);
}
