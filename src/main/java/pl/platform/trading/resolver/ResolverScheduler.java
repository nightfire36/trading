package pl.platform.trading.resolver;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.platform.trading.resolver.rates.ExchangeRatesProvider;

@Slf4j
@RequiredArgsConstructor
@Service
public class ResolverScheduler {

    private final TransactionResolver resolver;
    private final ExchangeRatesProvider rates;

    @Scheduled(fixedRateString = "${trading.rates-update-rate}", initialDelay = 0)
    private void updateRatesAndResolve() {
        try {
            rates.updateRates();
            resolver.resolve();
        } catch (Throwable t) {
            log.error("Exception during updating exchange rates. Exception details: ", t);
        }
    }
}
