package pl.platform.trading.resolver;

import pl.platform.trading.entity.ExchangeRate;
import pl.platform.trading.entity.position.OpenedPosition;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ProfitCalculator {
    public static BigDecimal calculateProfit(OpenedPosition opened, ExchangeRate rate) {
        if(opened == null || rate == null) {
            return null;
        }

        BigDecimal openingPrice = opened.getOpeningPrice();
        BigDecimal amount = opened.getAmount();

        return opened.getLongPosition() ?
                amount.multiply(rate.getBid().divide(openingPrice, 16, RoundingMode.HALF_UP))
                        .setScale(5, RoundingMode.HALF_UP)
                        .subtract(amount) :
                amount.multiply(openingPrice.divide(rate.getAsk(), 16, RoundingMode.HALF_UP))
                        .setScale(5, RoundingMode.HALF_UP)
                        .subtract(amount);
    }
}
