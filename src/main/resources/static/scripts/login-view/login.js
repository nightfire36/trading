import { displayRegisterView } from "../register-view/register.js";
import { sendXhrRequest } from "../utils/sendXhrRequest.js";
import { loginCheck } from "../utils/loginCheck.js";

import { INVALID_USERNAME_OR_PASSWORD, REGISTRATION_SUCCESSFUL, LOGOUT_SUCCESSFUL } from "./loginMessageCode.js";

export function displayLoginView(messageCode) {
	document.getElementById("root").innerHTML = htmlCode;

	switch(messageCode) {
		case INVALID_USERNAME_OR_PASSWORD:
			document.getElementById("message").innerHTML = `<font color="red">Invalid username or password!</font>`;
			break;
		case REGISTRATION_SUCCESSFUL:
			document.getElementById("message").innerHTML = `<font color="green">Registration successful.<br />Now you can log in to your account.</font>`;
			break;
		case LOGOUT_SUCCESSFUL:
			document.getElementById("message").innerHTML = `<font color="green">Logout successful.</font>`;
			break;
	}

	document.getElementById("link_create_account_1").onclick = displayRegisterView;
	document.getElementById("link_create_account_2").onclick = displayRegisterView;
	document.getElementById("button_submit").onclick = submitLoginForm;
}

function submitLoginForm() {
	let encodedUsername = encodeURIComponent(document.getElementById("input_email").value);
	let encodedPassword = encodeURIComponent(document.getElementById("input_password").value);
	
	sendXhrRequest(
		"POST", 
		"/user/login", 
		`username=${encodedUsername}&password=${encodedPassword}`,
		"application/x-www-form-urlencoded",
		() => loginCheck(INVALID_USERNAME_OR_PASSWORD)
	);
}

let htmlCode = 
	`<br />
	<br />
	<div class="card mx-auto" style="width: 36rem;">
		<h5 class="card-title mx-auto">Welcome on the webpage</h5>
		<div class="card-body">
		This trading platform was designed for Forex learning purposes. If you don't have an account 
		you need to <a href="#" id="link_create_account_1">create one for free</a>. It is necessary for maintaining continuity of 
		your transactions history and account balance.
		</div>
	</div>
	<br />
	<div class="card mx-auto" style="width: 24rem;">
		<h5 class="card-title mx-auto">Login</h5>
		<div class="card-body">
			<p id="message"></p>
			<br />
            <div class="form-group">
                <label>Email address</label>
                <input type="email" class="form-control" id="input_email" placeholder="Enter email">
                <small class="form-text text-muted">Enter your e-mail address.</small>
                <br />
                <label>Password</label>
                <input type="password" class="form-control" id="input_password" placeholder="Password">
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-primary" id="button_submit">Submit</button>
            </div>
            <div class="text-center">
                Don't have an account? <a href="#" id="link_create_account_2">Create for free</a>.
            </div>
		</div>
	</div>`;
