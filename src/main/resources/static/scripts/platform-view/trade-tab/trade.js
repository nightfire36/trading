import { setActivePlatformTab } from "../../utils/setActivePlatformTab.js";

import { generateChartForPair } from "./chartPanel.js";
import { generateNewPositionsPanel } from "./newPositionsPanel.js";
import { generateOpenedPositionsPanel } from "./openedPositionsPanel.js";
import { generatePendingOrdersPanel } from "./pendingOrdersPanel.js";

export function displayTradeTab() {
	document.getElementById("current_tab_content").innerHTML = htmlCode;
	setActivePlatformTab("trade_nav");
	
	reload();
	generateChartForPair('EURUSD');
}

function reload() {
	generateNewPositionsPanel();
	generateOpenedPositionsPanel();
	generatePendingOrdersPanel();

	setTimeout(reload, 60*1000);
}

let htmlCode = 
	`<div class="card-body">
		<h5 class="card-title">Trading panel</h5>
		<p class="card-text">
			<div class="container-fluid">
				<div class="row">
					<div class="col">
						<div class="card border-primary">
							<div class="card-header">Open new position</div>
							<p class="card-text">
								<p id = "new_positions">Loading content...</p>
							</p>
						</div>
					</div>
					<div class="col">
						<div class="row">
							<div class="card border-secondary">
								<div class="card-header">Chart</div>
								<p class="card-text">
									<p id = "chart">Loading content...</p>
								</p>
							</div>
						</div>
						<br />
						<div class="row">
							<div class="card border-secondary">
								<div class="card-header">Opened positions</div>
								<p class="card-text">
									<p id = "opened_positions">Loading content...</p>
								</p>
							</div>
						</div>
						<br />
						<div class="row">
							<div class="card border-secondary">
								<div class="card-header">Pending orders</div>
								<p class="card-text">
									<p id = "pending_orders">Loading content...</p>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</p>
	</div>`;
