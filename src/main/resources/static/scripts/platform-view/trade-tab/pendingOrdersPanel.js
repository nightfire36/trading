import { sendXhrRequest } from "../../utils/sendXhrRequest.js";
import { convertToLocaleDate } from "../../utils/tableUtilis.js";

function cancelOrder(clickedButton) {
	console.log("cancelOrder", clickedButton.value);

	sendXhrRequest(
		"POST", 
		`/api/cancel_order?orderId=${clickedButton.value}`,
		null,
		null,
		() => generatePendingOrdersPanel()
	);
}

function buildPendingOrdersRow(order) {
	var longPositionStr, triggeredAboveStr, orderId, amount;

	if(order.longPosition) {
		longPositionStr = `<font color="green">Long</font>`;
	}
	else {
		longPositionStr = `<font color="red">Short</font>`;
	}

	if(order.triggeredAbove) {
		triggeredAboveStr = `<font color="green">Above</font>`;
	}
	else {
		triggeredAboveStr = `<font color="red">Below</font>`;
	}

	if(order.id) {
		orderId = order.id;
	}
	else {
		orderId = "-";
	}

	if(order.amount) {
		amount = order.amount;
	}
	else {
		amount = "-";
	}

	var row = `
		<tr>
			<th scope="row">${orderId}</th>
			<td>${order.currencyPair}</td>
			<td>${amount}</td>
			<td>${order.orderPrice}</td>
			<td>${convertToLocaleDate(order.orderTimestamp)}</td>
			<td>${longPositionStr}</td>
			<td>${triggeredAboveStr}</td>
			<td>
				<button type="button" class="btn btn-outline-primary btn-sm" name="cancel_order_button" value="${orderId}">
					Cancel order
				</button>
			</td>
		</tr>`;
	
	return row;
}

function buildPendingOrdersPanel(json) {

	var tableCode = '';

	for(var i in json) {
		tableCode += buildPendingOrdersRow(json[i]);
	}

	var htmlCode = `
		<table class="table table-sm">
			<thead>
				<tr>
					<th scope="col">ID</th>
					<th scope="col">Currency pair</th>
					<th scope="col">Amount</th>
					<th scope="col">Order Price</th>
					<th scope="col">Order timestamp</th>
					<th scope="col">Position type</th>
					<th scope="col">Triggered</th>
					<th scope="col">Cancel order</th>
				</tr>
			</thead>
			<tbody>
				${tableCode}
			</tbody>
		</table>`;

	return htmlCode;
}

function pendingOrdersListener(xhrRequest) {
	console.log(xhrRequest.status);
	if(xhrRequest.readyState == 4 && xhrRequest.status == 200) {
		console.log("pending orders received successfully");
		if(xhrRequest.responseText[0] == '[') {
			var jsonOrders = JSON.parse(xhrRequest.responseText);
			document.getElementById("pending_orders").innerHTML = buildPendingOrdersPanel(jsonOrders);
			document.getElementsByName("cancel_order_button").forEach(
				e => e.onclick = () => cancelOrder(e)
			)
		}
	}
	else {
		console.log("pending orders receive failure");
	}
}

export function generatePendingOrdersPanel() {
	sendXhrRequest(
		"GET", 
		"/api/orders?orderStatus=P",
		null,
		null,
		pendingOrdersListener
	);
}
