import { setActivePlatformTab } from "../../utils/setActivePlatformTab.js";
import { sendXhrRequest } from "../../utils/sendXhrRequest.js";
import { generateRowPositionHtmlCode, valueOrPauseIfNull, convertToLocaleDate } from "../../utils/tableUtilis.js";

import { displayPositionsFilter } from "./positions_filter.js";
import { generatePositionsReport } from "./generate_positions_report.js";

export var receivedPositions;

export function displayOpenedPositionsTab() {
	document.getElementById("current_tab_content").innerHTML = htmlCode;
	setActivePlatformTab("opened_positions_nav");

	fetchPositions(
		positions => {
			receivedPositions = positions

			buildPositionsTable(positions);

			document.getElementById("show_positions_filter_button").onclick = displayPositionsFilter;
			document.getElementById("generate_positions_report_button").onclick = generatePositionsReport;
		}
	);
}

function fetchPositions(callback) {
	sendXhrRequest(
		"GET", 
		"/api/positions",
		null, 
		null, 
		xhrRequest => callback(JSON.parse(xhrRequest.response))
	);
}

export function buildPositionsTable(positions) {
	let tableHtmlCode = "";

	positions
		.sort((e1, e2) => e1.id > e2.id ? 1 : -1)
		.forEach(
			e => {
				tableHtmlCode += 
					`<tr>
						<th scope="row">${e.id}</th>
						<td>${e.currencyPair}</td>
						<td>${e.amount}</td>
						<td>${e.openingPrice}</td>
						<td>${convertToLocaleDate(e.openingTimestamp)}</td>
						<td>${generateRowPositionHtmlCode(e.longPosition)}</td>
						<td>${positionOpenedOrClosed(e)}</td>
						<td>${getProfitOrCurrentProfit(e)}</td>
						<td>${valueOrPauseIfNull(e.closingPrice)}</td>
						<td>${convertToLocaleDate(e.closingTimestamp)}</td>
					</tr>`;
			}
		);

	document.getElementById("opened_position_table").innerHTML = tableHtmlCode;
}

function positionOpenedOrClosed(position) {
	switch(position.positionStatus) {
		case "opened":
			return `<font color="green">Opened</font>`;
		case "closed":
			return `<font color="red">Closed</font>`;
		default:
			return "-";
	}
}

function getProfitOrCurrentProfit(position) {
	let profit = position.profit != null ? 
		position.profit : position.currentProfit;
	return profit > 0 ? 
		`<font color="green">${profit}</font>` : 
		profit < 0 ? 
			`<font color="red">${profit}</font>` : 
			profit;
}

let htmlCode = 
	`
	<div class="card-body mx-auto" style="width: 70rem;">
		<h5 class="card-title">Informations about opened positions</h5>
		<p class="card-text">
			<div align="left">
				<button type="button" class="btn btn-outline-primary btn-sm" id="show_positions_filter_button">
					Show filter
				</button>
				<button type="button" class="btn btn-outline-info btn-sm" id="apply_positions_filter_button" hidden>
					Apply filter
				</button>
				<button type="button" class="btn btn-outline-success btn-sm" id="generate_positions_report_button">
					Generate report
				</button>
			</div>
			<br />
			<form action="/report/positions_report" method="GET">
				<p id="positions_filter"></p>
				<input type="submit" id="submit_generate_positions_report" hidden>
			</form>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th scope="col">Transaction ID</th>
						<th scope="col">Currency pair</th>
						<th scope="col">Amount</th>
						<th scope="col">Opening price</th>
						<th scope="col">Opening time</th>
						<th scope="col">Position type</th>
						<th scope="col">Status</th>
						<th scope="col">Profit</th>
						<th scope="col">Closing price</th>
						<th scope="col">Closing time</th>
					</tr>
				</thead>
				<tbody id="opened_position_table"></tbody>
			</table>
		</p>
	</div>`;
