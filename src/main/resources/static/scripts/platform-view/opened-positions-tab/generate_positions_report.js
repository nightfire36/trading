import { verifyFilterCheckboxes } from "./positions_filter.js";

export function generatePositionsReport() {
    verifyFilterCheckboxes();
    document.getElementById("submit_generate_positions_report").click();
}
