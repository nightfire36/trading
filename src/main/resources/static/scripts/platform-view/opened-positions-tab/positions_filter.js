import { receivedPositions, displayOpenedPositionsTab, buildPositionsTable } from "./opened_positions.js";

import { uncheckWhenBothChecked, arrayFromCheckedByName, valueOfFirstCheckedByName } from "../../utils/filterUtils.js";

export function displayPositionsFilter() {
    document.getElementById("positions_filter").innerHTML = htmlCode;

    document.getElementById("show_positions_filter_button").textContent = "Hide filter";
    document.getElementById("show_positions_filter_button").classList.remove("btn-outline-primary");
    document.getElementById("show_positions_filter_button").classList.add("btn-outline-secondary");
    document.getElementById("show_positions_filter_button").onclick = displayOpenedPositionsTab;

    document.getElementById("apply_positions_filter_button").removeAttribute("hidden");
    document.getElementById("apply_positions_filter_button").onclick = applyPositionsFilter;
}

function applyPositionsFilter() {
    verifyFilterCheckboxes();

    let checkedCurrencyPairs = arrayFromCheckedByName("currencyPairs"); 

    let amountGreaterThan = document.getElementsByName("positionAmountGreaterThan")[0].value;
    let amountLowerThan = document.getElementsByName("positionAmountLowerThan")[0].value;

    let priceGreaterThan = document.getElementsByName("openingPriceGreaterThan")[0].value;
    let priceLowerThan = document.getElementsByName("openingPriceLowerThan")[0].value;

    let openingDateAfter = document.getElementsByName("openingDateAfter")[0].value;
    let openingDateBefore = document.getElementsByName("openingDateBefore")[0].value;
    let closingDateAfter = document.getElementsByName("closingDateAfter")[0].value;
    let closingDateBefore = document.getElementsByName("closingDateBefore")[0].value;

    let longPosition = valueOfFirstCheckedByName("longPosition");
    let openedPosition = valueOfFirstCheckedByName("openedPosition");

    let filteredPositions = receivedPositions
        .filter(e => !checkedCurrencyPairs || checkedCurrencyPairs.length == 0 || checkedCurrencyPairs.includes(e.currencyPair))
        .filter(e => !amountGreaterThan || parseFloat(e.amount) > parseFloat(amountGreaterThan))
        .filter(e => !amountLowerThan || parseFloat(e.amount) < parseFloat(amountLowerThan))
        .filter(e => !priceGreaterThan || parseFloat(e.openingPrice) > parseFloat(priceGreaterThan))
        .filter(e => !priceLowerThan || parseFloat(e.openingPrice) < parseFloat(priceLowerThan))
        .filter(e => !openingDateAfter || new Date(e.openingTimestamp) > new Date(openingDateAfter))
        .filter(e => !openingDateBefore || new Date(e.openingTimestamp) < new Date(openingDateBefore))
        .filter(e => !closingDateAfter || new Date(e.closingTimestamp) > new Date(closingDateAfter))
        .filter(e => !closingDateBefore || new Date(e.closingTimestamp) < new Date(closingDateBefore))
        .filter(e => !longPosition || e.longPosition == (longPosition.toLowerCase() == "true"))
        .filter(e => !openedPosition || e.positionStatus == (openedPosition.toLowerCase() == "true" ? "opened" : "closed"));

    buildPositionsTable(filteredPositions);
}

export function verifyFilterCheckboxes() {
    uncheckWhenBothChecked("long_position_true_checkbox", "long_position_false_checkbox");
    uncheckWhenBothChecked("opened_position_true_checkbox", "opened_position_false_checkbox");
}

let htmlCode = `
    <table class="table">
        <thead>
            <tr>
                <th>Currency pair</th>
                <th>Amount</th>
                <th>Opening price</th>
                <th>Opening date</th>
                <th>Closing date</th>
                <th>Position type</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="form-check" align="left">
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="EURUSD">
                        <label class="form-check-label">EURUSD</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="GBPUSD">
                        <label class="form-check-label">GBPUSD</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="EURPLN">
                        <label class="form-check-label">EURPLN</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="USDPLN">
                        <label class="form-check-label">USDPLN</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="USDCAD">
                        <label class="form-check-label">USDCAD</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="USDCHF">
                        <label class="form-check-label">USDCHF</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="AUDUSD">
                        <label class="form-check-label">AUDUSD</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="USDJPY">
                        <label class="form-check-label">USDJPY</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="EURJPY">
                        <label class="form-check-label">EURJPY</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="EURGBP">
                        <label class="form-check-label">EURGBP</label><br />
                    </div>
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Amount greater than" name="positionAmountGreaterThan"><br />
                    <input type="text" class="form-control" placeholder="Amount lower than" name="positionAmountLowerThan">
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Opening price greater than" name="openingPriceGreaterThan"><br />
                    <input type="text" class="form-control" placeholder="Opening price lower than" name="openingPriceLowerThan">
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Opened after" name="openingDateAfter"><br />
                    <input type="text" class="form-control" placeholder="Opened before" name="openingDateBefore">
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Closed after" name="closingDateAfter"><br />
                    <input type="text" class="form-control" placeholder="Closed before" name="closingDateBefore">
                </td>
                <td>
                    <div class="form-check" align="left">
                        <input type="checkbox" class="form-check-input" name="longPosition" value="true" id="long_position_true_checkbox">
                        <label class="form-check-label">Long</label><br />
                        <input type="checkbox" class="form-check-input" name="longPosition" value="false" id="long_position_false_checkbox">
                        <label class="form-check-label">Short</label><br />
                    </div>
                </td>
                <td>
                    <div class="form-check" align="left">
                        <input type="checkbox" class="form-check-input" name="openedPosition" value="true" id="opened_position_true_checkbox">
                        <label class="form-check-label">Opened</label><br />
                        <input type="checkbox" class="form-check-input" name="openedPosition" value="false" id="opened_position_false_checkbox">
                        <label class="form-check-label">Closed</label><br />
                    </div>
                </td>
            </tr>
        </tbody>
    </table>`;
