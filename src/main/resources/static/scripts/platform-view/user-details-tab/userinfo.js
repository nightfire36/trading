import { setActivePlatformTab } from "../../utils/setActivePlatformTab.js";
import { sendXhrRequest } from "../../utils/sendXhrRequest.js";
import { convertToLocaleDate } from "../../utils/tableUtilis.js";

export function displayUserDetailsTab() {
	document.getElementById("current_tab_content").innerHTML = htmlCode;
	setActivePlatformTab("user_info_nav");

	sendXhrRequest(
		"GET", 
		"/api/account_info", 
		null, 
		null, 
		function(xhrRequest) {
			let xhrResponse = JSON.parse(xhrRequest.response);
			document.getElementById("userinfo_first_name").innerHTML = xhrResponse.firstName;
			document.getElementById("userinfo_last_name").innerHTML = xhrResponse.lastName;
			document.getElementById("userinfo_email").innerHTML = xhrResponse.email;
			document.getElementById("userinfo_created_at").innerHTML = convertToLocaleDate(xhrResponse.createdAt);
			document.getElementById("userinfo_account_balance").innerHTML = xhrResponse.accountBalance + " PLN";
		}
	);
}

let htmlCode = 
	`<div class="card-body mx-auto" style="width: 38rem;">
		<h5 class="card-title">Informations about current user</h5>
		<p class="card-text">
			<table class="table table-striped">
				<tbody>
					<tr>
						<th scope="row">First name</th>
						<td id="userinfo_first_name">-</td>
					</tr>
					<tr>
						<th scope="row">Last name</th>
						<td id="userinfo_last_name">-</td>
					</tr>
					<tr>
						<th scope="row">E-mail</th>
						<td id="userinfo_email">-</td>
					</tr>
					<tr>
						<th scope="row">Date of creation</th>
						<td id="userinfo_created_at">-</td>
					</tr>
					<tr>
						<th scope="row">Account balance</th>
						<td id="userinfo_account_balance">-</td>
					</tr>
				</tbody>
			</table>
		</p>
	</div>`;
