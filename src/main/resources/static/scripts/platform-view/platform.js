import { displayPlatformDescriptionTab } from "./platform-description-tab/platform-description.js";
import { displayUserDetailsTab } from "./user-details-tab/userinfo.js";
import { displayOpenedPositionsTab } from "./opened-positions-tab/opened_positions.js";
import { displayPendingOrdersTab } from "./pending-orders-tab/pending_orders.js";
import { displayTradeTab } from "./trade-tab/trade.js";

import { sendXhrRequest } from "../utils/sendXhrRequest.js";
import { loginCheck } from "../utils/loginCheck.js";
import { LOGOUT_SUCCESSFUL } from "../login-view/loginMessageCode.js";

export function displayPlatformView() {
    document.getElementById("root").innerHTML = htmlCode;

    document.getElementById("platform_description_nav").onclick = displayPlatformDescriptionTab;
    document.getElementById("user_info_nav").onclick = displayUserDetailsTab;
    document.getElementById("opened_positions_nav").onclick = displayOpenedPositionsTab;
    document.getElementById("pending_orders_nav").onclick = displayPendingOrdersTab;
    document.getElementById("trade_nav").onclick = displayTradeTab;

    document.getElementById("button_logout").onclick = logout;
    
    displayPlatformDescriptionTab();
}

function logout() {
    sendXhrRequest(
        "POST",
    		`/user/logout`,
    		null,
    		null,
    		() => loginCheck(LOGOUT_SUCCESSFUL)
    );
}

let htmlCode = 
    `<div class="card text-center">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs" id="platform_header">
                <li class="nav-item">
                    <a class="nav-link" id="platform_description_nav">Platform description</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="user_info_nav">User info</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="opened_positions_nav">Positions</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pending_orders_nav">Orders</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="trade_nav">Trading panel</a>
                </li>
                <li class="nav-item ml-auto">
                    <button type="submit" class="btn btn-primary" id="button_logout">Logout</button>
                </li>
            </ul>
        </div>
        <div id="current_tab_content">
        </div>
    </div>`;