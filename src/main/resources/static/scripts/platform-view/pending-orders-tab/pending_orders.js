import { setActivePlatformTab } from "../../utils/setActivePlatformTab.js";
import { sendXhrRequest } from "../../utils/sendXhrRequest.js";
import { valueOrPauseIfNull, generateRowPositionHtmlCode, convertToLocaleDate } from "../../utils/tableUtilis.js";
import { PENDING_ORDER, EXECUTED, CANCELED, SYSTEM_CANCELED } from "../../utils/orderStatus.js";

import { displayOrdersFilter } from "./orders_filter.js";
import { generateOrdersReport } from "./generate_orders_report.js";

export var receivedOrders;

export function displayPendingOrdersTab() {
	document.getElementById("current_tab_content").innerHTML = htmlCode;
	setActivePlatformTab("pending_orders_nav");

	fetchOrders(orders => {
		receivedOrders = orders;

		buildOrdersTable(orders);

		document.getElementById("show_orders_filter_button").onclick = displayOrdersFilter;
		document.getElementById("generate_orders_report_button").onclick = generateOrdersReport;
	});
}

function fetchOrders(callback) {
	sendXhrRequest(
		"GET", 
		"/api/orders",
		null, 
		null, 
		xhrRequest => callback(JSON.parse(xhrRequest.response))
	);
}

export function buildOrdersTable(orders) {
	let tableHtmlCode = "";

	orders
		.sort((e1, e2) => e1.id > e2.id ? 1 : -1)
		.forEach(
			e => {
				tableHtmlCode += 
					`<tr>
						<th scope="row">${valueOrPauseIfNull(e.id)}</th>
						<td>${getOrderCurrencyPair(e)}</td>
						<td>${getOrderAmount(e)}</td>
						<td>${e.orderPrice}</td>
						<td>${convertToLocaleDate(e.orderTimestamp)}</td>
						<td>${getOrderTypeHtmlCode(e.orderType)}</td>
						<td>${generateRowPositionHtmlCode(getOrderLongPosition(e))}</td>
						<td>${generateRowTriggeredHtmlCode(e.triggeredAbove)}</td>
						<td>${getOrderStatus(e.orderStatus)}</td>
						<td>${getOrderRelatedPositionId(e)}</td>
					</tr>`;
			}
		);

	document.getElementById("pending_orders_table").innerHTML = tableHtmlCode;
}

export function getOrderCurrencyPair(order) {
	if(order.currencyPair != null) {
		return order.currencyPair;
	} else if(order.openedPosition != null) {
		return order.openedPosition.currencyPair;
	} else if(order.closedPosition != null) {
		return order.closedPosition.currencyPair;
	} else {
		return "-";
	}
}

export function getOrderAmount(order) {
	if(order.amount != null) {
		return order.amount;
	} else if(order.openedPosition != null) {
		return order.openedPosition.amount;
	} else if(order.closedPosition != null) {
		return order.closedPosition.amount;
	} else {
		return "-";
	}
}

export function getOrderLongPosition(order) {
	if(order.longPosition != null) {
		return order.longPosition;
	} else if(order.openedPosition != null) {
		return order.openedPosition.longPosition;
	} else if(order.closedPosition != null) {
		return order.closedPosition.longPosition;
	} else {
		return null;
	}
}

function getOrderRelatedPositionId(order) {
	switch(order.orderType) {
		case "opening":
			return order.openedPositionId != null ? 
				order.openedPositionId : "-";
		case "closing":
			return order.forexPositionId;
		default:
			return "-";
	}
}

function getOrderTypeHtmlCode(orderType) {
	switch(orderType) {
		case "opening":
			return `<font color="green">Opening</font>`;
		case "closing":
			return `<font color="red">Closing</font>`;
		default:
			return "-";
	}
}

function getOrderStatus(orderStatus) {
	switch(orderStatus) {
		case PENDING_ORDER:
			return "Pending";
		case EXECUTED:
			return "Executed";
		case CANCELED:
			return "Cancelled";
		case SYSTEM_CANCELED:
			return "Cancelled";
		default:
			return "-";
	}
}

function generateRowTriggeredHtmlCode(triggered) {
	return triggered == null ? 
		"-" : 
		triggered ? 
			`<font color="green">Above</font>` : 
			`<font color="red">Below</font>`;
}

let htmlCode = 
	`<div class="card-body mx-auto" style="width: 70rem;">
		<h5 class="card-title">Pending orders</h5>
		<p class="card-text">
			<div align="left">
				<button type="button" class="btn btn-outline-primary btn-sm" id="show_orders_filter_button">
					Show filter
				</button>
				<button type="button" class="btn btn-outline-info btn-sm" id="apply_orders_filter_button" hidden>
					Apply filter
				</button>
				<button type="button" class="btn btn-outline-success btn-sm" id="generate_orders_report_button">
					Generate report
				</button>
			</div>
			<br />
			<form action="/report/orders_report" method="GET">
				<p id="orders_filter"></p>
				<input type="submit" id="submit_generate_orders_report" hidden>
			</form>
			<table class="table table-striped table-sm">
				<thead>
					<tr>
						<th scope="col">Transaction ID</th>
						<th scope="col">Currency pair</th>
						<th scope="col">Amount</th>
						<th scope="col">Order price</th>
						<th scope="col">Order date</th>
						<th scope="col">Order type</th>
						<th scope="col">Position type</th>
						<th scope="col">Triggered</th>
						<th scope="col">Status</th>
						<th scope="col">Related position ID</th>
					</tr>
				</thead>
				<tbody id="pending_orders_table"></tbody>
			</table>
		</p>
	</div>`;
