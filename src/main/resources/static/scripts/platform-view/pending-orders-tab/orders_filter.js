import { 
    receivedOrders, displayPendingOrdersTab, buildOrdersTable, getOrderCurrencyPair, 
    getOrderAmount, getOrderLongPosition 
} from "./pending_orders.js";

import { uncheckWhenBothChecked, arrayFromCheckedByName, valueOfFirstCheckedByName } from "../../utils/filterUtils.js";

export function displayOrdersFilter() {
    document.getElementById("orders_filter").innerHTML = htmlCode;

    document.getElementById("show_orders_filter_button").textContent = "Hide filter";
    document.getElementById("show_orders_filter_button").classList.remove("btn-outline-primary");
    document.getElementById("show_orders_filter_button").classList.add("btn-outline-secondary");
    document.getElementById("show_orders_filter_button").onclick = displayPendingOrdersTab;

    document.getElementById("apply_orders_filter_button").removeAttribute("hidden");
    document.getElementById("apply_orders_filter_button").onclick = applyOrdersFilter;
}

function applyOrdersFilter() {
    verifyFilterCheckboxes();

    let checkedCurrencyPairs = arrayFromCheckedByName("currencyPairs");
    let checkedOrderStatuses = arrayFromCheckedByName("orderStatuses")
        .flatMap(e => e.split(","));

    let amountGreaterThan = document.getElementsByName("amountGreaterThan")[0].value;
    let amountLowerThan = document.getElementsByName("amountLowerThan")[0].value;

    let priceGreaterThan = document.getElementsByName("orderPriceGreaterThan")[0].value;
    let priceLowerThan = document.getElementsByName("orderPriceLowerThan")[0].value;

    let orderDateAfter = document.getElementsByName("orderDateAfter")[0].value;
    let orderDateBefore = document.getElementsByName("orderDateBefore")[0].value;

    let openingOrder = valueOfFirstCheckedByName("openingOrder");
    let longPosition = valueOfFirstCheckedByName("longPosition");
    let triggeredAbove = valueOfFirstCheckedByName("triggeredAbove");

    let filteredOrders = receivedOrders
        .filter(e => !checkedCurrencyPairs || checkedCurrencyPairs.length == 0 || checkedCurrencyPairs.includes(getOrderCurrencyPair(e)))
        .filter(e => !checkedOrderStatuses || checkedOrderStatuses.length == 0 || checkedOrderStatuses.includes(e.orderStatus))
        .filter(e => !amountGreaterThan || parseFloat(getOrderAmount(e)) > parseFloat(amountGreaterThan))
        .filter(e => !amountLowerThan || parseFloat(getOrderAmount(e)) < parseFloat(amountLowerThan))
        .filter(e => !priceGreaterThan || parseFloat(e.orderPrice) > parseFloat(priceGreaterThan))
        .filter(e => !priceLowerThan || parseFloat(e.orderPrice) < parseFloat(priceLowerThan))
        .filter(e => !orderDateAfter || new Date(e.orderTimestamp) > new Date(orderDateAfter))
        .filter(e => !orderDateBefore || new Date(e.orderTimestamp) < new Date(orderDateBefore))
        .filter(e => !openingOrder || e.orderType == (openingOrder.toLowerCase() == "true" ? "opening" : "closing"))
        .filter(e => !longPosition || getOrderLongPosition(e) == (longPosition.toLowerCase() == "true"))
        .filter(e => !triggeredAbove || e.triggeredAbove == (triggeredAbove.toLowerCase() == "true"));

    buildOrdersTable(filteredOrders);
}

export function verifyFilterCheckboxes() {
    uncheckWhenBothChecked("long_position_true_checkbox", "long_position_false_checkbox");
    uncheckWhenBothChecked("opening_order_true_checkbox", "opening_order_false_checkbox");
    uncheckWhenBothChecked("triggered_above_true_checkbox", "triggered_above_false_checkbox");
}

let htmlCode = `
    <table class="table">
        <thead>
            <tr>
                <th>Currency pair</th>
                <th>Amount</th>
                <th>Order price</th>
                <th>Order date</th>
                <th>Order type</th>
                <th>Position type</th>
                <th>Triggered</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <div class="form-check" align="left">
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="EURUSD">
                        <label class="form-check-label">EURUSD</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="GBPUSD">
                        <label class="form-check-label">GBPUSD</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="EURPLN">
                        <label class="form-check-label">EURPLN</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="USDPLN">
                        <label class="form-check-label">USDPLN</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="USDCAD">
                        <label class="form-check-label">USDCAD</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="USDCHF">
                        <label class="form-check-label">USDCHF</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="AUDUSD">
                        <label class="form-check-label">AUDUSD</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="USDJPY">
                        <label class="form-check-label">USDJPY</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="EURJPY">
                        <label class="form-check-label">EURJPY</label><br />
                        <input type="checkbox" class="form-check-input" name="currencyPairs" value="EURGBP">
                        <label class="form-check-label">EURGBP</label><br />
                    </div>
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Amount greater than" name="amountGreaterThan"><br />
                    <input type="text" class="form-control" placeholder="Amount lower than" name="amountLowerThan">
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Price greater than" name="orderPriceGreaterThan"><br />
                    <input type="text" class="form-control" placeholder="Price lower than" name="orderPriceLowerThan">
                </td>
                <td>
                    <input type="text" class="form-control" placeholder="Placed after" name="orderDateAfter"><br />
                    <input type="text" class="form-control" placeholder="Placed before" name="orderDateBefore">
                </td>
                <td>
                    <div class="form-check" align="left">
                        <input type="checkbox" class="form-check-input" name="openingOrder" value="true" id="opening_order_true_checkbox">
                        <label class="form-check-label">Opening</label><br />
                        <input type="checkbox" class="form-check-input" name="openingOrder" value="false" id="opening_order_false_checkbox">
                        <label class="form-check-label">Closing</label><br />
                    </div>
                </td>
                <td>
                    <div class="form-check" align="left">
                        <input type="checkbox" class="form-check-input" name="longPosition" value="true" id="long_position_true_checkbox">
                        <label class="form-check-label">Long</label><br />
                        <input type="checkbox" class="form-check-input" name="longPosition" value="false" id="long_position_false_checkbox">
                        <label class="form-check-label">Short</label><br />
                    </div>
                </td>
                <td>
                    <div class="form-check" align="left">
                        <input type="checkbox" class="form-check-input" name="triggeredAbove" value="true" id="triggered_above_true_checkbox">
                        <label class="form-check-label">Above</label><br />
                        <input type="checkbox" class="form-check-input" name="triggeredAbove" value="false" id="triggered_above_false_checkbox">
                        <label class="form-check-label">Below</label><br />
                    </div>
                </td>
                <td>
                    <div class="form-check" align="left">
                        <input type="checkbox" class="form-check-input" name="orderStatuses" value="P">
                        <label class="form-check-label">Pending</label><br />
                        <input type="checkbox" class="form-check-input" name="orderStatuses" value="E">
                        <label class="form-check-label">Executed</label><br />
                        <input type="checkbox" class="form-check-input" name="orderStatuses" value="C,A">
                        <label class="form-check-label">Cancelled</label><br />
                    </div>
                </td>
            </tr>
        </tbody>
    </table>`;
