import { verifyFilterCheckboxes } from "./orders_filter.js";

export function generateOrdersReport() {
    verifyFilterCheckboxes();
    document.getElementById("submit_generate_orders_report").click();
}
