import { loginCheck } from "./utils/loginCheck.js";
import { initializeKeyEventListeners } from "./utils/initializeKeyEventListeners.js";

window.onload = () => {
    initializeKeyEventListeners();
    loginCheck();
}
