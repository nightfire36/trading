export function generateRowPositionHtmlCode(longPosition) {
    return longPosition == null ? 
        "-" :
        longPosition ? 
            `<font color="green">Long</font>` : 
            `<font color="red">Short</font>`;
}

export function generateRowTransactionProfitHtmlCode(profit) {
	return profit > 0 ? 
		`<font color="green">${profit}</font>` :
		profit < 0 ? 
			`<font color="red">${profit}</font>` : 
			`${profit}`;
}

export function valueOrPauseIfNull(value) {
	return value != null ? 
		value :
		"-";
}

export function convertToLocaleDate(dateStr) {
	let date = new Date(dateStr);
	return !isNaN(date) ? 
		date.toLocaleString() : "-";
}