export const PENDING_ORDER = 'P';
export const EXECUTED = 'E';
export const CANCELED = 'C';
export const SYSTEM_CANCELED = 'A';