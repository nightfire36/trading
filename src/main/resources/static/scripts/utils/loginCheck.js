import { sendXhrRequest } from "./sendXhrRequest.js";
import { displayLoginView } from "../login-view/login.js";
import { displayPlatformView } from "../platform-view/platform.js";
import { initCsrfToken } from "./initCsrfToken.js";

export function loginCheck(messageCode) {
    initCsrfToken();
    
    sendXhrRequest(
        "GET", 
        "/user/login_check", 
        null, 
        null, 
        xhrRequest => xhrRequest.status == 200 ? 
            displayPlatformView() : displayLoginView(messageCode)
    );
}