export function uncheckWhenBothChecked(checkbox1, checkbox2) {
    if(
        document.getElementById(checkbox1) != null &&
        document.getElementById(checkbox2) != null &&
        document.getElementById(checkbox1).checked && 
        document.getElementById(checkbox2).checked
    ) {
        document.getElementById(checkbox1).checked = false;
        document.getElementById(checkbox2).checked = false;
    }
}

export function arrayFromCheckedByName(elementName) {
    return Array.from(document.querySelectorAll(`[name="${elementName}"]`))
        .filter(e => e.checked)
        .map(e => e.value);
}

export function valueOfFirstCheckedByName(elementName) {
    return Array.from(document.querySelectorAll(`[name="${elementName}"]`))
        .filter(e => e.checked)
        .map(e => e.value)[0];
}