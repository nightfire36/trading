import { sendXhrRequest } from "./sendXhrRequest.js";

export var csrfToken;

export function initCsrfToken() {
    sendXhrRequest(
        "GET", 
        "/user/csrf_token", 
        null, 
        null, 
        xhrRequest => csrfToken = xhrRequest.response
    );
}