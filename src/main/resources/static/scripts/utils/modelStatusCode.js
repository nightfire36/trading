// default success code
export const STATUS_SUCCESS = 10;

// error codes

// common error codes
export const ERROR_CANNOT_FIND_ELEMENT = 20;

// take position error codes
export const ERROR_NOT_ENOUGH_MONEY = 101;

// place order error codes
export const ERROR_DB_SAVE_EXCEPTION = 201;

// close position error codes
export const ERROR_DB_SET_ORDER_STATUS_EXCEPTION = 301;
export const ERROR_BELONGS_TO_ANOTHER_USER = 302;

// cancel order error codes

// registration error codes
export const ERROR_EMAIL_ALREADY_EXISTS = 501;
export const ERROR_INVALID_USER_DATA = 502;
export const ERROR_INVALID_FIELD_VALUE = 503;
export const ERROR_CONSTRAINT_VIOLATION = 504;

// login error codes
export const ERROR_BAD_CREDENTIALS = 601;
export const ERROR_UNKNOWN_LOGIN_ERROR = 602;
