import { csrfToken } from "./initCsrfToken.js";

export function sendXhrRequest(httpMethod, endpoint, body, contentType, callback) {
    let xhrRequest = new XMLHttpRequest();

    if(callback != null) {
        xhrRequest.addEventListener("load", () => callback(xhrRequest));
    }

    xhrRequest.open(httpMethod, endpoint, true);

    if(contentType != null) {
        xhrRequest.setRequestHeader("Content-Type", contentType);
    }

    if(httpMethod.toUpperCase() == "POST") {
        xhrRequest.setRequestHeader("x-csrf-token", csrfToken);
    }

    xhrRequest.send(body);
}
