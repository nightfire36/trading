export function setActivePlatformTab(tabId) {
    let navLinks = document.getElementById("platform_header").getElementsByClassName("nav-link");
    Array.from(navLinks).forEach(
        element => element.classList.remove("active")
    );
    document.getElementById(tabId).classList.add("active");
}
