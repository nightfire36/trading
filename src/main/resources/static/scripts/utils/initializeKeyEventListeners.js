export function initializeKeyEventListeners() {
    document.addEventListener(
        "keyup",
        event => event.code == "Enter" ? document.getElementById("button_submit").click() : null
    );
}