import { displayLoginView } from "../login-view/login.js";
import { sendXhrRequest } from "../utils/sendXhrRequest.js";

import { REGISTRATION_SUCCESSFUL } from "../login-view/loginMessageCode.js";
import { MESSAGE_EMAIL_ALREADY_EXISTS, MESSAGE_REGISTRATION_FAILED } from "./registerMessageCode.js";

import { STATUS_SUCCESS, ERROR_INVALID_FIELD_VALUE, ERROR_EMAIL_ALREADY_EXISTS } from "../utils/modelStatusCode.js";

import { INVALID_FIRST_NAME, INVALID_LAST_NAME, INVALID_EMAIL, INVALID_PASSWORD } from "./invalidFieldCode.js";


export function displayRegisterView(messageCode, invalidFields) {
    document.getElementById("root").innerHTML = htmlCode;

    document.getElementById("first_name_help").innerHTML = `Minimum 3 letters.`;
    document.getElementById("last_name_help").innerHTML = `Minimum 3 letters.`;
    document.getElementById("email_help").innerHTML = `Must be valid e-mail address.`;
    document.getElementById("password_help").innerHTML = `Must consist of minimum 6 signs.`;

    if(messageCode == MESSAGE_EMAIL_ALREADY_EXISTS) {
	    document.getElementById("message").innerHTML = `<font color="red">Registration failed! Account with provided <br /> e-mail address already exists.</font>`;
	} else if(messageCode == MESSAGE_REGISTRATION_FAILED) {
	    document.getElementById("message").innerHTML = `<font color="red">Registration failed!</font>`;

        if(invalidFields != null && invalidFields != 0) {
            if(invalidFields & INVALID_FIRST_NAME) {
                document.getElementById("first_name_help").innerHTML = `<font color="red">Invalid first name!</font>`;
            }
            if(invalidFields & INVALID_LAST_NAME) {
                document.getElementById("last_name_help").innerHTML = `<font color="red">Invalid last name!</font>`;
            }
            if(invalidFields & INVALID_EMAIL) {
                document.getElementById("email_help").innerHTML = `<font color="red">Invalid email!</font>`;
            }
            if(invalidFields & INVALID_PASSWORD) {
                document.getElementById("password_help").innerHTML = `<font color="red">Invalid password!</font>`;
            }
        }
    }
    
    document.getElementById("link_login").onclick = displayLoginView;

    document.getElementById("button_submit").onclick = submitRegisterForm;
}

function submitRegisterForm() {
	sendXhrRequest(
		"POST", 
		"/user/register",
		JSON.stringify(
            {
                firstName: document.getElementById("input_first_name").value,
                lastName: document.getElementById("input_last_name").value,
                email: document.getElementById("input_email").value,
                password: document.getElementById("input_password").value,
            }
        ),
		"application/json",
		xhrRequest => {
            if(xhrRequest.status == 200) {
                let xhrResponse = JSON.parse(xhrRequest.response);
                switch(xhrResponse.status) {
                    case STATUS_SUCCESS:
                        displayLoginView(REGISTRATION_SUCCESSFUL);
                        break;
                    case ERROR_INVALID_FIELD_VALUE:
                        displayRegisterView(MESSAGE_REGISTRATION_FAILED, xhrResponse.invalidFields);
                        break;
                    case ERROR_EMAIL_ALREADY_EXISTS:
                        displayRegisterView(MESSAGE_EMAIL_ALREADY_EXISTS, null);
                        break;
                    default:
                        displayRegisterView(MESSAGE_REGISTRATION_FAILED, null);
                        break;
                }
            }
        }
	);
}

let htmlCode =
    `<div class="card mx-auto" style="width: 24rem;">
        <h5 class="card-title mx-auto">Create new account</h5>
        <div class="card-body">
            <p id="message"></p>
            <div class="form-group">
                <label>First name</label>
                <input type="text" class="form-control" id="input_first_name" placeholder="First name">
                <small id="first_name_help" class="form-text text-muted"></small>
                <br />
                <label>Last name</label>
                <input type="text" class="form-control" id="input_last_name" placeholder="Last name">
                <small id="last_name_help" class="form-text text-muted"></small>
                <br />
                <label>Email address</label>
                <input type="email" class="form-control" id="input_email" placeholder="Enter email">
                <small id="email_help" class="form-text text-muted"></small>
                <br />
                <label>Password</label>
                <input type="password" class="form-control" id="input_password" placeholder="Password">
                <small id="password_help" class="form-text text-muted"></small>
            </div>
            <div class="text-center">
                <button class="btn btn-primary" id="button_submit">Submit</button>
            </div>
            <div class="text-center">
                Already have an account? <a href="#" id="link_login">Sign in</a>
            </div>
        </div>
    </div>`;
