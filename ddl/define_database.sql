create database trading;

create user 'trading_app'@'localhost' identified by 'trading_db_pass_1234';

grant insert, select, update on trading.* to 'trading_app'@'localhost';

use trading;

create table user (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL,
    password VARCHAR(60) NOT NULL,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    account_balance DECIMAL(14,4),
    status INT,
    UNIQUE KEY(email)
);

create table forex_position (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT,
    currency_pair CHAR(6),
    amount DECIMAL (14,4),
    opening_price DECIMAL (12,5),
    long_position BOOLEAN,
    dtype CHAR,
    opening_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    closing_price DECIMAL (12,5),
    profit DECIMAL(14,4),
    closing_timestamp TIMESTAMP,
    to_archive_flag CHAR DEFAULT 'N',
    FOREIGN KEY(user_id) REFERENCES user(id)
);

create table transaction_order (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    user_id BIGINT,
    forex_position_id BIGINT,
    currency_pair CHAR(6),
    amount DECIMAL (14,4),
    order_price DECIMAL (12,5),
    dtype CHAR,
    order_status CHAR,
    order_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    long_position BOOLEAN,
    triggered_above BOOLEAN,
    opened_position_id BIGINT,
    execution_timestamp TIMESTAMP,
    to_archive_flag CHAR DEFAULT 'N',
    FOREIGN KEY(forex_position_id) REFERENCES forex_position(id),
    FOREIGN KEY(opened_position_id) REFERENCES forex_position(id),
    FOREIGN KEY(user_id) REFERENCES user(id)
);

create table exchange_rate (
    id BIGINT AUTO_INCREMENT PRIMARY KEY,
    currency_pair CHAR(6),
    bid DECIMAL (14,4),
    ask DECIMAL (14,4),
    timestamp TIMESTAMP,
    server_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

create table archived_position (
    id BIGINT PRIMARY KEY,
    user_id BIGINT,
    currency_pair CHAR(6),
    amount DECIMAL (14,4),
    opening_price DECIMAL (12,5),
    long_position BOOLEAN,
    dtype CHAR,
    opening_timestamp TIMESTAMP,
    closing_price DECIMAL (12,5),
    profit DECIMAL(14,4),
    closing_timestamp TIMESTAMP,
    FOREIGN KEY(user_id) REFERENCES user(id)
);

create table archived_order (
    id BIGINT PRIMARY KEY,
    user_id BIGINT,
    archived_position_id BIGINT,
    currency_pair CHAR(6),
    amount DECIMAL (14,4),
    order_price DECIMAL (12,5),
    dtype CHAR,
    order_status CHAR,
    order_timestamp TIMESTAMP,
    long_position BOOLEAN,
    triggered_above BOOLEAN,
    opened_position_id BIGINT,
    execution_timestamp TIMESTAMP,
    FOREIGN KEY(archived_position_id) REFERENCES archived_position(id),
    FOREIGN KEY(opened_position_id) REFERENCES archived_position(id),
    FOREIGN KEY(user_id) REFERENCES user(id)
);
